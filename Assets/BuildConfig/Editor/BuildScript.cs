﻿using System;
using System.IO;
using System.Linq;
using UnityEditor;
using UnityEditor.Build.Content;
using UnityEngine;

class BuildScript
{
    private const string k_projectName = "Puzzle Tombs";
    
    [MenuItem("File/Build For All Targets")]
    public static void BuildForAllTargets()
    {
        BuildWindows();
        BuildWebGL();
        BuildAndroid();
    }
    
    [MenuItem("File/Build For Android")]
    static void BuildAndroid()
    {
        string keystoreFilename = "KeystorePassword.txt";

        string filePath = Path.GetFullPath(Application.dataPath + "/../" + keystoreFilename);

        Debug.Log("Checking: "+filePath);
        
        if (File.Exists(filePath))
        {
            StreamReader configReader = new StreamReader(filePath);
            string storePassword = configReader.ReadLine();
            configReader.Close();
 
            PlayerSettings.keystorePass = storePassword;
            PlayerSettings.keyaliasPass = storePassword;

            PlayerSettings.Android.targetArchitectures = AndroidArchitecture.ARMv7 | AndroidArchitecture.ARM64;
            PlayerSettings.Android.buildApkPerCpuArchitecture = true;

            //Shrug, this is what unity set it to after upgrading to 2020
            PlayerSettings.Android.minifyDebug = true;
            PlayerSettings.Android.minifyRelease = true;
            //PlayerSettings.Android.minifyWithR8 = false; //this falls over so I've disabled it
            
            //This is what it was set to before. I can't remember why.
            //EditorUserBuildSettings.androidDebugMinification = AndroidMinification.Proguard;
            //EditorUserBuildSettings.androidReleaseMinification = AndroidMinification.Proguard;
            
            Debug.Log("Password length: "+storePassword.Length);
        }
        else
        {
            Debug.Log("No keystore password provided");
        }
        
        build(BuildTarget.Android, BuildOptions.None);
    }
    
    [MenuItem("File/Build For WebGL")]
    static void BuildWebGL()
    {
        build(BuildTarget.WebGL, BuildOptions.Development);
    }
    
    [MenuItem("File/Build For Windows")]
    static void BuildWindows()
    {
        build(BuildTarget.StandaloneWindows, BuildOptions.Development);
    }
    
    [MenuItem("File/Build For OSX")]
    static void BuildOSX()
    {
        build(BuildTarget.StandaloneOSX, BuildOptions.Development);
    }
    
    private static void build(BuildTarget target, BuildOptions options)
    {
        string fileExtension = "";

        switch (target)
        {
            case BuildTarget.StandaloneOSX:
            case BuildTarget.StandaloneOSXIntel:
                fileExtension = ".app";
                break;
            case BuildTarget.StandaloneWindows:
                fileExtension = ".exe";
                break;
            case BuildTarget.iOS:
                break;
            case BuildTarget.Android:
                fileExtension = ".apk";
                break;
            case BuildTarget.StandaloneLinux:
                break;
            case BuildTarget.StandaloneWindows64:
                break;
            case BuildTarget.WebGL:
                break;
            case BuildTarget.WSAPlayer:
                break;
            case BuildTarget.StandaloneLinux64:
                break;
            case BuildTarget.StandaloneLinuxUniversal:
                break;
            case BuildTarget.WP8Player:
                break;
            case BuildTarget.StandaloneOSXIntel64:
                break;
            case BuildTarget.Switch:
                break;
            default:
                throw new ArgumentOutOfRangeException(nameof(target), target, null);
        }

        string buildFolder = "bin/" + target.ToString("G");

        string buildPath;
        
        if (target == BuildTarget.Android)
        {
            buildPath = buildFolder;
        }
        else
        {
            buildPath = buildFolder+"/"+k_projectName+fileExtension;
        }
        
        string[] scenes = EditorBuildSettings.scenes.Select(s => s.path).ToArray();

        // Create build folder if it doesn't yet exist
        if(!Directory.Exists(buildFolder))
            Directory.CreateDirectory(buildFolder);

        BuildPipeline.BuildPlayer(scenes, buildPath, target, options);
    }
}