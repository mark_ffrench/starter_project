﻿using System.Collections;
using System.Linq;
using Framework;
using Helpers;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class SoundtrackController : MonoBehaviour
{
    private AudioSource audioSource;
    
    [SerializeField] private AudioClip[] audioClips;
    [SerializeField] private bool randomiseOrderAfterFirstClip;
    
    public bool Abort = false;
    private bool Started = false;

    private float MaxVolume = 1f;
    private float StartVolume = 1f;
    private float TargetVolume = 1f;
    private float FadeDuration = 1f;
    private float FadeElapsed = 0f;
    
    private void Awake()
    {
        audioSource = GetComponent<AudioSource>();
        MaxVolume = audioSource.volume;
    }

    void Start()
    {
        if(Started)
            return;
        
        if (randomiseOrderAfterFirstClip)
        {
            var randomised = audioClips.Skip(1).Shuffle();
            audioClips = audioClips.Take(1).Concat(randomised).ToArray();
        }

        if (PrivacyAndSettings.GetMusicSetting())
        {
            StartPlaying();
        }
    }

    public void StartPlaying()
    {
        Abort = false;
        Started = true;
        
        StartCoroutine(PlayAudioSequentially());
    }

    public void StopPlaying()
    {
        Abort = true;
    }

    private IEnumerator PlayAudioSequentially()
    {
        yield return null;

        int i = 0;

        while (!Abort)
        {
            audioSource.clip = audioClips[i];
            audioSource.Play();

            while (audioSource.isPlaying && !Abort)
            {
                yield return null;
            }

            i = (i + 1) % audioClips.Length;
        }
        
        audioSource.Stop();

        yield return null;
    }

    private IEnumerator FadeCoroutine()
    {
        WaitForEndOfFrame wait = new WaitForEndOfFrame();
        FadeElapsed = 0f;

        while (FadeElapsed < FadeDuration)
        {
            float t = FadeElapsed / FadeDuration;
            audioSource.volume = Mathf.Lerp(StartVolume, TargetVolume, t);

            FadeElapsed += Time.unscaledDeltaTime;
            yield return wait;
        }
    }
    
    public void FadeOut()
    {
        StartVolume = audioSource.volume;
        TargetVolume = 0f;
        StartCoroutine(FadeCoroutine());
    }

    public void FadeIn()
    {
        StartVolume = audioSource.volume;
        TargetVolume = MaxVolume;
        StartCoroutine(FadeCoroutine());
    }
}