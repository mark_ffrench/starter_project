﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StarburstRotate : MonoBehaviour
{
    [SerializeField] private float rotationSpeed;

    private RectTransform rectTransform;
    
    private void Awake()
    {
        rectTransform = GetComponent<RectTransform>();
    }

    // Update is called once per frame
    void Update()
    {
        rectTransform.Rotate(0, 0, -rotationSpeed * Time.deltaTime);
    }
}
