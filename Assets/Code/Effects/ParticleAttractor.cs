using System;
using UnityEngine;

[ExecuteInEditMode]
public class ParticleAttractor : MonoBehaviour
{
    public Transform m_target;
    public Vector3 m_offset;
    public Space m_space;
    public bool m_blendRotation;
    public AnimationCurve m_blendOverLifetime = new AnimationCurve(new Keyframe(0, 0), new Keyframe(1f, 1f));

    [SerializeField] private ParticleSystem m_particleSystem;
    private ParticleSystem.Particle[] m_particles;
    
    private Vector3 targetPosition;
    
    private void Awake()
    {
        if (m_particleSystem == null)
            this.enabled = false;
    }

    private void LateUpdate()
    {
        if ((m_target == null) || (m_particleSystem == null) || (!m_particleSystem.isPlaying))
            return;

        InitializeIfNeeded();

        int numParticlesAlive = m_particleSystem.GetParticles(m_particles);

        for (int i = 0; i < numParticlesAlive; i++)
        {
            float lifetimeNormalised = Mathf.Clamp01((m_particles[i].startLifetime - m_particles[i].remainingLifetime) / m_particles[i].startLifetime);

            // position
            targetPosition = m_space == Space.World ? m_target.position + m_offset : m_target.TransformPoint(m_offset);

            m_particles[i].position = Vector3.Lerp(m_particles[i].position, targetPosition, m_blendOverLifetime.Evaluate(lifetimeNormalised));
            
            // rotation
            if (m_blendRotation)
                m_particles[i].rotation3D = Vector3.Lerp(m_particles[i].rotation3D, m_target.eulerAngles, m_blendOverLifetime.Evaluate(lifetimeNormalised));
        }

        m_particleSystem.SetParticles(m_particles, numParticlesAlive);
    }

    private void InitializeIfNeeded()
    {
        if (m_particles == null || m_particles.Length < m_particleSystem.main.maxParticles)
            m_particles = new ParticleSystem.Particle[m_particleSystem.main.maxParticles];
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.DrawSphere(targetPosition, 10f);
    }
}