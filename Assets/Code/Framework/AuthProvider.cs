using System;
using System.Threading.Tasks;
using Facebook.Unity;
using Firebase.Auth;
using UnityEngine;

namespace UI
{
    public class AuthProvider : MonoBehaviour
    {
        private FirebaseAuth auth => FirebaseAuth.DefaultInstance;
        public event Action OnSignedIn;

        //holding on to this so that it doesn't get GCed, as that would cause client to be unauthenticated
        public static FirebaseUser User;

        public void Awake()
        {
            //you must access DefaultInstance and lazy-load it, or the app will crash when you attempt login
            //(Or at least it does with Facebook)
            FirebaseAuth auth = FirebaseAuth.DefaultInstance;
        }

        public async Task AnonLogin()
        {
            Debug.Log($"Current user = {auth.CurrentUser}");

            var task = auth.SignInAnonymouslyAsync();

            User = await task;

            if (task.Exception != null)
            {
                Debug.LogException(task.Exception);
            }

            Debug.Log($"Signed in anonymously with userID {User.UserId}");

            OnSignedIn?.Invoke();
        }
        
        public async Task GooglePlayLogin(string authCode)
        {
            Credential credential = PlayGamesAuthProvider.GetCredential(authCode);
                    
            auth.SignInWithCredentialAsync(credential).ContinueWith(task => {
                if (task.IsCanceled) {
                    Debug.LogError("SignInWithCredentialAsync was canceled.");
                    return;
                }
                if (task.IsFaulted) {
                    Debug.LogError("SignInWithCredentialAsync encountered an error: " + task.Exception);
                    return;
                }

                User = task.Result;
                Debug.Log($"User signed in successfully: {User.DisplayName} ({User.UserId})");
                
                OnSignedIn?.Invoke();
            });
        }

        public static string DisplayName()
        {
            return User.DisplayName;
        }

        public static string UserID()
        {
            if (User == null)
            {
                return null;
            }
            
            return User.UserId;
        }

        public void FacebookLogin(AccessToken accessToken)
        {
            Credential credential = FacebookAuthProvider.GetCredential(accessToken.TokenString);
            
            auth.SignInWithCredentialAsync(credential).ContinueWith(task => {
                if (task.IsCanceled) {
                    Debug.LogError("SignInWithCredentialAsync was canceled.");
                    return;
                }
                if (task.IsFaulted) {
                    Debug.LogError("SignInWithCredentialAsync encountered an error: " + task.Exception);
                    return;
                }

                User = task.Result;
                Debug.Log($"User signed in successfully: {User.DisplayName} ({User.UserId})");
                
                OnSignedIn?.Invoke();
            });
        }
    }
}