using Helpers;
using UnityEngine;
using UnityEngine.Analytics;
using UnityEngine.Advertisements;

namespace Framework
{
    public class AdsProvider_Unity : AdsProvider, IUnityAdsInitializationListener, IUnityAdsLoadListener, IUnityAdsShowListener
    {
#if UNITY_ANDROID
        private string GameID = "4366397";
        private string RewardedAdPlacementUnity = "Rewarded_Android";
        private string InterstitialAdPlacementUnity = "Interstitial_Android";
#elif UNITY_IOS
        private string GameID = "4366396";
        private string RewardedAdPlacementUnity = "Rewarded_iOS";
        private string InterstitialAdPlacementUnity = "Interstitial_iOS";
#else
        private string GameID = "NOTSET";
        private string RewardedAdPlacementUnity = "NOTSET";
        private string InterstitialAdPlacementUnity = "NOTSET";
#endif

#if DEVELOPMENT || INTERNAL
        private const bool TestMode = true;
#else
        private const bool TestMode = false;
#endif


        protected override AdvertisingNetwork GetNetwork()
        {
            return AdvertisingNetwork.UnityAds;
        }

        protected override void Init()
        {
            bool enablePerPlacementMode = false;
            
            Advertisement.Initialize(GameID, TestMode, enablePerPlacementMode, this);
        }
        
        public void OnInitializationComplete()
        {
            Debug.Log("Unity Ads initialization complete.");
            PrewarmAds();
        }
        
        public override void PrewarmAds()
        {
            Debug.Log("Prewarming ads");
            Advertisement.Load(ContinuePlacement, this);
            Advertisement.Load(RewardedAdPlacementUnity, this);
            Advertisement.Load(InterstitialAdPlacementUnity, this);
        }

        
        public void OnInitializationFailed(UnityAdsInitializationError error, string message)
        {
            Debug.LogError($"Unity Ads Initialization Failed: {error.ToString()} - {message}");
        }
        protected override void Shutdown()
        {
            
        }

        protected override void ShowInterstitialImpl()
        {
#if UNITY_EDITOR
            Invoke(nameof(InterstitialAdClosed), 5f);
#endif
            Advertisement.Show(InterstitialAdPlacementUnity, this);
        }

        protected override void ShowCoinDoublerImpl()
        {
            Advertisement.Show(RewardedAdPlacementUnity, this);
        }

        protected override void ShowContinueImpl()
        {
            Advertisement.Show(ContinuePlacement, this);
        }

        protected override void ShowFreeEnergyImpl()
        {
            Advertisement.Show(RewardedAdPlacementUnity, this);
        }

        public override void ApplyPrivacySettings(OptInSetting optInSetting)
        {
            if(optInSetting == OptInSetting.NotSet)
                return;
            
            MetaData gdprMetaData = new MetaData("gdpr");

            if (optInSetting == OptInSetting.OptIn)
            {
                gdprMetaData.Set("consent", "true");
            }
            else if (optInSetting == OptInSetting.OptOut)
            {
                gdprMetaData.Set("consent", "false");
            }

            Advertisement.SetMetaData(gdprMetaData);
        }

        public override bool IsCoinDoublerAdAvailable()
        {
            return Advertisement.IsReady(RewardedAdPlacementUnity);
        }

        public override bool IsInterstitialAdAvailable()
        {
            return Advertisement.IsReady(InterstitialAdPlacementUnity);
        }

        public override bool IsContinueAdAvailable()
        {
            return Advertisement.IsReady(RewardedAdPlacementUnity);
        }

        public override bool IsEnergyAdAvailable()
        {
            return Advertisement.IsReady(RewardedAdPlacementUnity);
        }
        
        public void OnUnityAdsAdLoaded(string placementId)
        {
            Debug.Log($"Loaded {placementId} ad unit");
        }

        public void OnUnityAdsFailedToLoad(string placementId, UnityAdsLoadError error, string message)
        {
            //it seems like I always get a timeout loading test ads, so I'm just downgrading the error to a log
            if (error == UnityAdsLoadError.TIMEOUT && TestMode)
            {
                Debug.Log($"Error loading Ad Unit: {placementId} - {error.ToString()} - {message}");
            }
            else
            {
                Debug.LogError($"Error loading Ad Unit: {placementId} - {error.ToString()} - {message}");
            }
        }

        public void OnUnityAdsShowFailure(string placementId, UnityAdsShowError error, string message)
        {
            Debug.LogError($"Error showing Ad Unit: {placementId} - {error.ToString()} - {message}");
            
        }

        public void OnUnityAdsShowStart(string placementId)
        {
            Debug.Log($"Started {placementId} ad unit");
        }

        public void OnUnityAdsShowClick(string placementId)
        {
            Debug.Log($"Clicked {placementId} ad unit");
        }

        public void OnUnityAdsShowComplete(string placementId, UnityAdsShowCompletionState showCompletionState)
        {
            Debug.Log($"Completed {placementId} ad unit {showCompletionState}");

            if (showCompletionState == UnityAdsShowCompletionState.COMPLETED)
            {
                //give reward
                if (placementId == RewardedAdPlacementUnity)
                {
                    pendingRewards.Add(new Reward(){Amount = 1, Type = RewardTypeDoubleCoins});
                }
                else if (placementId == ContinuePlacement)
                {
                    pendingRewards.Add(new Reward(){Amount = 1, Type = RewardTypeContinue});
                }
                else if (placementId == FreeEnergyPlacement)
                {
                    pendingRewards.Add(new Reward(){Amount = 50, Type = CurrencyAccount.Energy});
                }
            }

            if (placementId == InterstitialAdPlacementUnity)
            {
                ThreadHelper.ExecuteOnMainThread(InterstitialAdClosed);
            }
            
            Advertisement.Load(placementId, this);
        }
    }
}