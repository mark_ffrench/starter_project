﻿public enum OptInSetting
{
    NotSet = 0,
    OptIn = 1,
    OptOut = 2
}