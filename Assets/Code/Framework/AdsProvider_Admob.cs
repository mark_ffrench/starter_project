using System.Collections.Generic;
using GoogleMobileAds.Api;
using Helpers;
using UnityEngine;
using UnityEngine.Analytics;

namespace Framework
{
    public class AdsProvider_Admob : AdsProvider
    {
        private RewardedAd continueAd;
        private RewardedAd freeEnergyAd;
        private RewardedAd coinDoublerAd;
        private InterstitialAd interstitialAd;

        protected override AdvertisingNetwork GetNetwork()
        {
            throw new System.NotImplementedException();
        }

        protected override void Init()
        {
#if UNITY_EDITOR
            PrewarmAds();
#else
            MobileAds.Initialize(OnInitialised);
#endif
        }

        private void OnInitialised(InitializationStatus status)
        {
            Debug.Log("Ads initialised " + status);
        
            //this accesses player age via player prefs, so do it on the main thread
            ThreadHelper.ExecuteOnMainThread(PrewarmAds);
        }
        
        protected override void Shutdown()
        {
            
        }

        protected override void ShowInterstitialImpl()
        {
            ServeAd(interstitialAd);
        }

        protected override void ShowCoinDoublerImpl()
        {
            ServeAd(coinDoublerAd);
        }

        protected override void ShowContinueImpl()
        {
            ServeAd(continueAd);
        }

        protected override void ShowFreeEnergyImpl()
        {
            ServeAd(freeEnergyAd);
        }

        //received on a separate thread, but reward must be handled on the main thread
        private void HandleUserEarnedReward(object sender, GoogleMobileAds.Api.Reward args)
        {
            pendingRewards.Add(new Reward(){Amount = (int)args.Amount, Type = args.Type});
        }

        public override void PrewarmAds()
        {
            List<string> testDevices = new List<string>
            {
                AdRequest.TestDeviceSimulator,
                TestDevicePixel3
            };
        
            var requestConfigBuilder = new RequestConfiguration.Builder();
            
            if (PrivacyAndSettings.IsUserAChild())
            {
                Debug.Log("Opting out of personalised ads because the user is a child");
            
                requestConfigBuilder.SetTagForChildDirectedTreatment(TagForChildDirectedTreatment.True) //COPPA-related (13)
                    .SetTagForUnderAgeOfConsent(TagForUnderAgeOfConsent.True); //GDPR-related (also 13, although higher in some countries)
            }
            else
            {
                Debug.Log("Not automatically opting out of personalised ads because of user's age");
            
                //tag for child directed treatment (TFCD) = COPPA
                //tag for under age of consent (TFUA) = GDPR
            
                requestConfigBuilder.SetTagForChildDirectedTreatment(TagForChildDirectedTreatment.False)
                    .SetTagForUnderAgeOfConsent(TagForUnderAgeOfConsent.False);
            }
            
            //pretty sure this just has to match the store listing, rather than interpreting the age they give
            requestConfigBuilder.SetMaxAdContentRating(MaxAdContentRating.G)
                .SetTestDeviceIds(testDevices);
            
            RequestConfiguration configuration = requestConfigBuilder.build();
            MobileAds.SetRequestConfiguration(configuration);
        
            coinDoublerAd = LoadRewardedAd(CoinDoublerPlacement);
            interstitialAd = LoadInterstitialAd(InterstitialPlacement);
        }

        private InterstitialAd LoadInterstitialAd(string placement)
        {
            var ad = new InterstitialAd(placement);
            ad.LoadAd(CreateAdRequest());
            return ad;
        }

        private RewardedAd LoadRewardedAd(string placement)
        {
            var ad = new RewardedAd(placement);
            ad.LoadAd(CreateAdRequest());
            return ad;
        }

        public override void ApplyPrivacySettings(OptInSetting optInSetting)
        {
            //this is handled in CreateAdRequest
            PrewarmAds();
        }

        public override bool IsCoinDoublerAdAvailable()
        {
            return coinDoublerAd.IsLoaded();
        }

        public override bool IsInterstitialAdAvailable()
        {
            return interstitialAd.IsLoaded();
        }
        
        public override bool IsContinueAdAvailable()
        {
            return continueAd.IsLoaded();
        }
        
        public override bool IsEnergyAdAvailable()
        {
            return freeEnergyAd.IsLoaded();
        }

        public AdRequest CreateAdRequest()
        {
            var adRequestBuilder = new AdRequest.Builder();

            OptInSetting setting = PrivacyAndSettings.GetOptInSetting();

            switch (setting)
            {
                case OptInSetting.OptOut:
                    adRequestBuilder.AddExtra("npa", "1");
                    break;
                case OptInSetting.NotSet:
                case OptInSetting.OptIn:
                    break;
            }

            //limited data
            adRequestBuilder.AddExtra("rdp", "1");

            return adRequestBuilder.Build();
        }
        
        public void ServeAd(InterstitialAd ad)
        {
            if (ad.IsLoaded())
            {
                ad.Show();
            }
            else
            {
                InterstitialAdFailedToShow();
            }
        }
    
        public void ServeAd(RewardedAd ad)
        {
            if (ad.IsLoaded())
            {
                ad.Show();
            }
            else
            {
                AnalyticsProvider.Get.LogEvent("NoRewardedAdAvailable");
                Debug.LogError("Ad was not ready to be shown");
            }
        }
    }
}