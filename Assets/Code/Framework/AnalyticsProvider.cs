﻿using System;
using System.Collections.Generic;
using System.Linq;
using AppsFlyerSDK;
using Firebase.Analytics;
using Helpers;
using UI;
using UnityEngine;
using UnityEngine.Analytics;
using UnityEngine.Purchasing;
using UnityEngine.Purchasing.Security;
using Object = UnityEngine.Object;

namespace Framework
{
    public class AnalyticsProvider
    {
        private const string DevAmplitudeAPIKey = "-";
        private const string ProdAmplitudeAPIKey = "-";

        private readonly Amplitude amplitude;

        private static AnalyticsProvider instance;
        private string currentAdPlacement = null;
        private AdPlacementType currentAdPlacementType;
        private AdvertisingNetwork currentAdNetwork;

        public static AnalyticsProvider Get
        {
            get => instance ?? (instance = new AnalyticsProvider());

            private set => instance = value;
        }

        public AnalyticsProvider()
        {
            AttributionProvider appsFlyer = new GameObject().AddComponent<AttributionProvider>();
            Object.DontDestroyOnLoad(appsFlyer);
            appsFlyer.Init();
            
            amplitude = Amplitude.getInstance();
            amplitude.setServerUrl("https://api2.amplitude.com");
            amplitude.logging = true;

            if (PrivacyAndSettings.IsUserAChild())
            {
                Debug.Log("User is a child");
                amplitude.enableCoppaControl();
            }

            bool optout = PrivacyAndSettings.HasUserOptedOutOfTracking();

            if (optout)
            {
                Debug.Log("User has opted out of tracking");
            }

            amplitude.setOptOut(optout);
            amplitude.trackSessionEvents(true);

#if DEVELOPMENT
            Debug.Log("Using dev amplitude key");
            amplitude.init(DevAmplitudeAPIKey);
#else
            Debug.Log("Using prod amplitude key");
            amplitude.init(ProdAmplitudeAPIKey);
#endif

            Get = this;
        }
        
#region APPSFLYER
        public void onConversionDataSuccess(string conversionData)
        {
            AppsFlyer.AFLog("onConversionDataSuccess", conversionData);
            Dictionary<string, object> conversionDataDictionary = AppsFlyer.CallbackStringToDictionary(conversionData);
            // add deferred deeplink logic here
        }

        public void onConversionDataFail(string error)
        {
            AppsFlyer.AFLog("onConversionDataFail", error);
        }

        public void onAppOpenAttribution(string attributionData)
        {
            AppsFlyer.AFLog("onAppOpenAttribution", attributionData);
            Dictionary<string, object> attributionDataDictionary = AppsFlyer.CallbackStringToDictionary(attributionData);
            // add direct deeplink logic here
        }

        public void onAppOpenAttributionFailure(string error)
        {
            AppsFlyer.AFLog("onAppOpenAttributionFailure", error);
        }
#endregion

        public void OnUserUpdated(UserSave save)
        {
            amplitude.setUserProperty("Coins", save.numCoins);
            amplitude.setUserProperty("AdsRemoved", save.AdsRemoved);
            amplitude.setUserProperty("DaysPlayed", save.NumDaysPlayed);
            amplitude.setUserProperty("Sessions", save.NumSessions);
            amplitude.setUserProperty("IntersitialAds", save.InterstitialsWatched);
            amplitude.setUserProperty("RewardedAds", save.RewardedAdsWatched);
            amplitude.setUserProperty("DailyPuzzles", save.NumDailyPuzzles);
            amplitude.setUserProperty("Purchases", save.NumPurchases);
            amplitude.setUserProperty("EstimatedSpend", (float)save.EstimatedSpend);
        }
        
        public void Login(string userID)
        {
            if (userID != null)
            {
                amplitude.setUserId(userID);
                FirebaseAnalytics.SetUserId(userID);
            }

            amplitude.logEvent("Login");
            FirebaseAnalytics.LogEvent(FirebaseAnalytics.EventLogin);
        }

        public void RecordNewSession(LoginResult loginResult)
        {
            Dictionary<string, object> payload = new Dictionary<string, object>();

            if (loginResult.Success)
            {
                payload["MinsSinceLastSession"] =
                    Mathf.FloorToInt((float) loginResult.TimeSinceLastSession.TotalMinutes);
                payload["HoursSinceLastSession"] =
                    Mathf.FloorToInt((float) loginResult.TimeSinceLastSession.TotalHours);
            }
            
            amplitude.logEvent("SessionDelta", payload);
        }

        public void LogEvent(string eventName, Dictionary<string, object> payload = null)
        {
            //amplitude
            amplitude.logEvent(eventName, payload);

            //firebase
            Parameter[] parameters = GetAsParameters(payload).ToArray();
            FirebaseAnalytics.LogEvent(eventName, parameters);

            //Unity
            AnalyticsEvent.Custom(eventName, TrimPayloadForUnity(payload));
        }

        public void StartLevel(string level, Dictionary<string, object> payload = null)
        {
            //Unity
            //This has a level complete event that takes the level ID directly
            AnalyticsEvent.LevelStart(level, TrimPayloadForUnity(payload));

            if (payload == null)
            {
                payload = new Dictionary<string, object>();
            }

            //for everything else it gets added to the payload
            payload.Add("LevelID", level);

            //amplitude
            amplitude.logEvent("LevelStart", payload);

            //firebase
            Parameter[] parameters = GetAsParameters(payload).ToArray();
            FirebaseAnalytics.LogEvent("LevelStart", parameters);
        }

        public void FailLevel(string level, Dictionary<string, object> payload = null)
        {
            //Unity
            //This has a level complete event that takes the level ID directly
            AnalyticsEvent.LevelFail(level, TrimPayloadForUnity(payload));

            if (payload == null)
            {
                payload = new Dictionary<string, object>();
            }

            //for everything else it gets added to the payload
            payload.Add("LevelID", level);

            //amplitude
            amplitude.logEvent("LevelFail", payload);

            //firebase
            Parameter[] parameters = GetAsParameters(payload).ToArray();
            FirebaseAnalytics.LogEvent("LevelFail", parameters);
        }

        public void QuitLevel(string level, Dictionary<string, object> payload = null)
        {
            //Unity
            //This has a level complete event that takes the level ID directly
            AnalyticsEvent.LevelFail(level, TrimPayloadForUnity(payload));

            if (payload == null)
            {
                payload = new Dictionary<string, object>();
            }

            //for everything else it gets added to the payload
            payload.Add("LevelID", level);

            //amplitude
            amplitude.logEvent("LevelQuit", payload);

            //firebase
            Parameter[] parameters = GetAsParameters(payload).ToArray();
            FirebaseAnalytics.LogEvent("LevelQuit", parameters);
        }

        public void CompleteLevel(string level, Dictionary<string, object> payload = null)
        {
            //Unity
            //This has a level complete event that takes the level ID directly
            AnalyticsEvent.LevelComplete(level, TrimPayloadForUnity(payload));

            if (payload == null)
            {
                payload = new Dictionary<string, object>();
            }

            //for everything else it gets added to the payload
            payload.Add("LevelID", level);

            //amplitude
            amplitude.logEvent("LevelComplete", payload);

            //firebase
            Parameter[] parameters = GetAsParameters(payload).ToArray();
            FirebaseAnalytics.LogEvent("LevelComplete", parameters);
        }

        public void EarnCurrency(string source, string currencyName, long amount)
        {
            //There's lots that I'm not doing properly here:
            AnalyticsEvent.ItemAcquired(AcquisitionType.Soft, "context", (float)amount, source);

            Dictionary<string, object> payload = new Dictionary<string, object>();
            payload.Add("Currency", currencyName);
            payload.Add("Amount", amount);
            payload.Add("EarnedFrom", source);

            amplitude.logEvent("CurrencyEarned", payload);

            //firebase
            FirebaseAnalytics.LogEvent(FirebaseAnalytics.EventEarnVirtualCurrency,
                new Parameter(FirebaseAnalytics.ParameterValue, amount),
                new Parameter(FirebaseAnalytics.ParameterVirtualCurrencyName, currencyName));
        }

        public void SpendCurrency(string itemPurchased, string currencyName, long amount)
        {
            //There's lots that I'm not doing properly here:
            AnalyticsEvent.ItemSpent(AcquisitionType.Soft, "context", (float)amount, itemPurchased);

            Dictionary<string, object> payload = new Dictionary<string, object>();
            payload.Add("Currency", currencyName);
            payload.Add("Amount", amount);
            payload.Add("SpentOn", itemPurchased);

            amplitude.logEvent("CurrencySpent", payload);

            //firebase
            FirebaseAnalytics.LogEvent(FirebaseAnalytics.EventSpendVirtualCurrency,
                new Parameter(FirebaseAnalytics.ParameterItemName, itemPurchased),
                new Parameter(FirebaseAnalytics.ParameterValue, amount),
                new Parameter(FirebaseAnalytics.ParameterVirtualCurrencyName, currencyName));
        }

        private IEnumerable<Parameter> GetAsParameters(Dictionary<string, object> payload)
        {
            if (payload == null)
            {
                yield break;
            }
            
            foreach (KeyValuePair<string, object> kvp in payload)
            {
                if (kvp.Value is long l)
                {
                    yield return new Parameter(kvp.Key, l);
                }

                if (kvp.Value is double d)
                {
                    yield return new Parameter(kvp.Key, d);
                }

                if (kvp.Value is string s)
                {
                    yield return new Parameter(kvp.Key, s);
                }
            }
        }

        private Dictionary<string, object> TrimPayloadForUnity(Dictionary<string, object> fullPayload)
        {
            Dictionary<string, object> trimmed = new Dictionary<string, object>();

            if (fullPayload != null)
            {
                foreach (KeyValuePair<string, object> kvp in fullPayload)
                {
                    trimmed.Add(kvp.Key, kvp.Value);

                    if (trimmed.Count == 9)
                        break;
                }
            }

            return trimmed;
        }

        public void PurchaseItemWithCurrency(StoreProductSingleItem item)
        {
            Dictionary<string, object> payload = new Dictionary<string, object>();

            payload.Add("BoughtType", item.payoutType);
            payload.Add("BoughtAmount", item.payoutAmount);

            payload.Add("SpentType", item.costType);
            payload.Add("SpentAmount", item.costAmount);

            payload.Add("ItemID", item.id);

            amplitude.logEvent("PurchaseItem", payload);
        }
        
        public void PurchaseFailed(Product product, PurchaseFailureReason failureReason)
        {
            Dictionary<string, object> payload = new Dictionary<string, object>();
            payload.Add("ProductID", product.definition.id);
            payload.Add("FailureReason", failureReason);
        }
        
        public void PurchaseVerified(Product argsPurchasedProduct, IPurchaseReceipt purchaseReceipt)
        {
            //string productId, int quantity, double price, string receipt, string receiptSignature
            amplitude.logRevenue(purchaseReceipt.productID, 1, (double)argsPurchasedProduct.metadata.localizedPrice, argsPurchasedProduct.receipt, "");
        }

        public void PurchaseUnverified(Product product)
        {
            Dictionary<string, object> payload = new Dictionary<string, object>();
            payload.Add("ProductID", product.definition.id);
            payload.Add("FailureReason", "Unverified");
            //amplitude.logRevenue(argsPurchasedProduct.definition.id, 1, (double)argsPurchasedProduct.metadata.localizedPrice, argsPurchasedProduct.receipt, "");
        }

        public enum AdPlacementType
        {
            Interstitial,
            Rewarded
        }

        public void OfferRewardedAd(string placement)
        {
            //Unity
            AnalyticsEvent.AdOffer(true);
            
            //Amplitude
            Dictionary<string, object> payload = new Dictionary<string, object>();
            payload.Add("Placement", placement);
            amplitude.logEvent("AdOffered", payload);
            
            //Firebase
            Parameter[] parameters = {
                new Parameter(FirebaseAnalytics.ParameterAdUnitName, currentAdPlacement),
            };
            
            FirebaseAnalytics.LogEvent("AdOffered", parameters);
        }

        public void StartAd(string placement, AdvertisingNetwork network, AdPlacementType placementType)
        {
            currentAdPlacement = placement;
            currentAdNetwork = network;
            currentAdPlacementType = placementType;
            
            //Unity
            AnalyticsEvent.AdStart(placementType == AdPlacementType.Rewarded, AdvertisingNetwork.Google, currentAdPlacement);

            //Amplitude
            Dictionary<string, object> payload = new Dictionary<string, object>();
            payload.Add("Placement", currentAdPlacement);
            payload.Add("Network", currentAdNetwork);
            payload.Add("Format", currentAdPlacementType);
            amplitude.logEvent("AdStart", payload);

            if (network != AdvertisingNetwork.Google)
            {
                //Firebase
                Parameter[] parameters =
                {
                    new Parameter(FirebaseAnalytics.ParameterAdUnitName, currentAdPlacement),
                    new Parameter(FirebaseAnalytics.ParameterAdPlatform, currentAdNetwork.ToString()),
                    new Parameter(FirebaseAnalytics.ParameterAdFormat, currentAdPlacement),
                    //new Parameter(FirebaseAnalytics.ParameterAdSource, ""),
                    //new Parameter(FirebaseAnalytics.ParameterAdNetworkClickID, ""),
                };

                FirebaseAnalytics.LogEvent(FirebaseAnalytics.EventAdImpression, parameters);
            }
        }

        public void RewardedAdFinished()
        {
            //Unity
            AnalyticsEvent.AdComplete(true, AdvertisingNetwork.Google, currentAdPlacement);
            
            //Amplitude
            Dictionary<string, object> payload = new Dictionary<string, object>();
            payload.Add("Placement", currentAdPlacement);
            payload.Add("Network", currentAdNetwork);
            payload.Add("Format", currentAdPlacementType);
            amplitude.logEvent("AdWatched", payload);
            
            //Firebase
            Parameter[] parameters = {
                new Parameter(FirebaseAnalytics.ParameterAdUnitName, currentAdPlacement),
                new Parameter(FirebaseAnalytics.ParameterAdPlatform, currentAdNetwork.ToString()),
                new Parameter(FirebaseAnalytics.ParameterAdFormat, currentAdPlacement),
                //new Parameter(FirebaseAnalytics.ParameterAdSource, ""),
                //new Parameter(FirebaseAnalytics.ParameterAdNetworkClickID, ""),
            };
            
            FirebaseAnalytics.LogEvent(FirebaseAnalytics.EventAdImpression,parameters);
            
            currentAdPlacement = null;
        }
        
        public void AdClosed()
        {
            //Unity
            //AnalyticsEvent.AdComplete(true, AdvertisingNetwork.Google, currentAdPlacement);
            
            //Amplitude
            Dictionary<string, object> payload = new Dictionary<string, object>();
            payload.Add("Placement", currentAdPlacement);
            payload.Add("Network", currentAdNetwork);
            payload.Add("Format", currentAdPlacementType);
            amplitude.logEvent("AdClosed", payload);
            
            currentAdPlacement = null;
        }

        public void GoToScreen<T>() where T : UIScreen
        {
            var type = typeof(T);
            AnalyticsEvent.ScreenVisit(type.ToString());

            amplitude.logEvent("GoTo_"+type);
        }
        
        public void ShowPopup<T>() where T : UIPopup
        {
            var type = typeof(T);
            AnalyticsEvent.ScreenVisit(type.ToString());

            amplitude.logEvent("ShowPopup_"+type);
        }

        public void OnBootFinished(float bootTime, LoginResult lastLoginResult)
        {
            Dictionary<string, object> payload = new Dictionary<string, object>();

            payload.Add("BootTime", Mathf.CeilToInt(bootTime));
            payload.Add("DaysSinceLastSession", Mathf.FloorToInt((float)lastLoginResult.TimeSinceLastSession.TotalDays));
            payload.Add("HoursSinceLastSession", Mathf.FloorToInt((float)lastLoginResult.TimeSinceLastSession.TotalHours));
            payload.Add("MinsSinceLastSession", Mathf.FloorToInt((float)lastLoginResult.TimeSinceLastSession.TotalMinutes));
            payload.Add("IsNewDay", lastLoginResult.IsNewDay);
            
            amplitude.logEvent("BootFinished", payload);
        }
    }
}