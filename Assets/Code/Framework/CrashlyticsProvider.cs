using System.Threading.Tasks;
using UnityEngine;
using Firebase;

public class CrashlyticsProvider : MonoBehaviour
{
    public bool ready;
    public bool unavailable;

    private FirebaseApp app;
    
    public async Task Initialize()
    {
#if DEVELOPMENT
        FirebaseApp.LogLevel = LogLevel.Debug;
#else
        FirebaseApp.LogLevel = LogLevel.Error;
#endif
        
        var dependencyStatus = await FirebaseApp.CheckAndFixDependenciesAsync();
        
        if (dependencyStatus == DependencyStatus.Available)
        {
            // Create and hold a reference to your FirebaseApp,
            // where app is a Firebase.FirebaseApp property of your application class.
            // Crashlytics will use the DefaultInstance, as well;
            // this ensures that Crashlytics is initialized.
            app = FirebaseApp.DefaultInstance;
                
            // Set a flag here for indicating that your project is ready to use Firebase.
            Debug.Log("Firebase initialized successfully");
            ready = true;
        }
        else
        {
            Debug.LogError($"Could not resolve all Firebase dependencies: {dependencyStatus}");
            // Firebase Unity SDK is not safe to use here.
            unavailable = true;
        }
    }
}