using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Helpers;
using UnityEngine;

public class Localisation
{
    private Dictionary<LocalisationKeys, string> currentLanguage = new Dictionary<LocalisationKeys, string>();
    private Dictionary<string, string> currentLanguageStringKeyed = new Dictionary<string, string>();

    public static Localisation Instance { get; private set; }

    public Localisation()
    {
        if (Instance != null)
        {
            return;
        }
        
        Instance = this;
    }
    
    public void LoadLanguage(LanguageCode languageCode)
    {
        currentLanguage.Clear();
        currentLanguageStringKeyed.Clear();
        
        TextAsset assets = Resources.Load<TextAsset>("Localisation/"+languageCode);

        int lineNum = -1;
                
        foreach (string fullLine in assets.text.GetLines())
        {
            lineNum++;

            if (string.IsNullOrEmpty(fullLine))
            {
                continue;
            }
            
            string[] line = fullLine.Split(new[]{','}, 2);
            
            if (line.Length != 2)
            {
                Debug.LogException(new Exception($"Invalid {languageCode} loc on line {lineNum} {line.Length}"));
                continue;
            }
            
            string key = line[0];

            if (string.IsNullOrEmpty(key))
            {
                Debug.LogException(new Exception($"Localisation: {languageCode} has null key on line {lineNum}"));
                continue;
            }
            
            string text = line[1];
            text = text.Replace("\"", "");
            
            if (string.IsNullOrEmpty(text))
            {
                Debug.LogException(new Exception($"Localisation: {languageCode} has null string for key {key} on line {lineNum}"));
                continue;
            }

            if (Enum.TryParse(key, out LocalisationKeys keyEnum))
            {
                currentLanguage.Add(keyEnum, text);
                currentLanguageStringKeyed.Add(key, text);
            }
            else
            {
                currentLanguageStringKeyed.Add(key, text);
            }

        }
        
        Debug.Log($"Finished loading loc... {currentLanguage.Count} keyed by enum, {currentLanguageStringKeyed.Count} keyed by string");
    }
    
    public bool KeyExists(string key)
    {
        return currentLanguageStringKeyed.ContainsKey(key);
    }
    
    public string Get(string key)
    {
        if (currentLanguageStringKeyed.TryGetValue(key, out string text))
        {
            return text;
        }
        
        Debug.LogException(new Exception($"Loc key {key} does not exist in the current language"));
        return key;
    }
    
    public string Get(LocalisationKeys key)
    {
        if (currentLanguage.TryGetValue(key, out string text))
        {
            return text;
        }

        Debug.LogException(new Exception($"Loc key {key} does not exist in the current language"));
        return key.ToString();
    }
    
    public string Get(LocalisationKeys key, params string[] parameters)
    {
        if (currentLanguage.TryGetValue(key, out string text))
        {
            return string.Format(text, parameters);
        }

        Debug.LogException(new Exception($"Loc key {key} does not exist in the current language"));
        return key.ToString();
    }
}

public enum LanguageCode
{
    EN
}