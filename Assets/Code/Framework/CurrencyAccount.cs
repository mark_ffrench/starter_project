using System;
using Framework;
using UnityEngine;

public class CurrencyAccount
{
    public const string Coins = "coins";
    public const string Gems = "gems";
    public const string Energy = "energy";
    public const string XP = "xp";

    public const int SecondsPerEnergy = 72;
    public const int MaxEnergy = 100;

    private string m_type;
    private int m_amount;
    public int Amount => m_amount;

    public event Action<int> OnSetOrSubtracted;
    public event Action<int, string> OnGainedFromSource;

    private UserSave save;

    public CurrencyAccount(string type, UserSave save)
    {
        this.save = save;
        
        m_type = type;
        m_amount = save.GetCurrency(type);
    }

    public void Add(int amount, string source = null)
    {
        int newAmount = m_amount + amount;
        
        //avoid cloud saving if there is no change
        if (newAmount == m_amount) 
            return;

        m_amount = newAmount;
        
        save.SetCurrency(m_type, m_amount);
        OnGainedFromSource?.Invoke(m_amount, source);

        if (source == null)
        {
            source = "Unknown";
        }
        
        AnalyticsProvider.Get.EarnCurrency(source, m_type, amount);
    }
    
    public void AddUpToLimit(int amount, int max, string source)
    {
        int newAmount;
        
        //if amount is already higher than max value, avoid accidentally clamping it
        if (m_amount >= max)
        {
            newAmount = m_amount;
        }
        else
        {
            newAmount = Math.Min(m_amount + amount, max);
        }
        
        //avoid cloud saving if there is no change
        if (newAmount == m_amount) 
            return;

        m_amount = newAmount;
        
        save.SetCurrency(m_type, m_amount);
        OnGainedFromSource?.Invoke(m_amount, source);

        AnalyticsProvider.Get.EarnCurrency(source, m_type, amount);
    }

    public void Set(int amount)
    {
        if (m_amount == amount)
            return;

        m_amount = amount;
        save.SetCurrency(m_type, m_amount);
        OnSetOrSubtracted?.Invoke(m_amount);
    }

    public bool TrySubtract(int amount, string itemName = null)
    {
        if (itemName == null)
            itemName = m_type;

        if (amount > m_amount)
            return false;

        if (amount != 0)
        {
            m_amount -= amount;
            save.SetCurrency(m_type, m_amount);
            OnSetOrSubtracted?.Invoke(m_amount);
            
            AnalyticsProvider.Get.SpendCurrency(itemName, m_type, amount);
        }
        
        return true;
    }

    /// <summary>
    /// If you don't have enough, spend until you reach zero and then return the amount spent.
    /// For example, using all your remaining energy to work on something (without completing it)
    /// </summary>
    /// <param name="cost">The amount that you'd like to spend</param>
    /// <returns>The amount that was actually spent</returns>
    public int SubtractPartial(int cost, string itemName = null)
    {
        int amountSubtracted = Math.Min(m_amount, cost);

        if (amountSubtracted > 0)
        {
            m_amount -= amountSubtracted;
            save.SetCurrency(m_type, m_amount);
            OnSetOrSubtracted?.Invoke(m_amount);
            
            AnalyticsProvider.Get.SpendCurrency(itemName, m_type, amountSubtracted);
        }

        return amountSubtracted;
    }

    public static string FormatString(string currency, int amount)
    {
        switch (currency)
        {
            case Coins:
                return $"<sprite index=0>{amount}";
            default:
                Debug.LogError("No symbol set for currency type"+currency);
                return amount.ToString();
        }
    }
}