﻿using System;
using System.Collections.Generic;
using System.Linq;
using Framework;
using UI;
using UnityEngine;
using UnityEngine.Analytics;
using UnityEngine.Purchasing;
using UnityEngine.Purchasing.Security;

public class IAPProvider : MonoBehaviour, IStoreListener
{
    public static IAPProvider Instance { get; private set; }

    private static IStoreController m_StoreController; // The Unity Purchasing system.
    private static IExtensionProvider m_StoreExtensionProvider; // The store-specific Purchasing subsystems.
    private static Product product = null;
    
    private const string GEMS_80 = "gems_80";
    private const string GEMS_230 = "gems_230";
    private const string GEMS_500 = "gems_500";
    private const string GEMS_1100 = "gems_1100";
    private const string GEMS_2100 = "gems_2100";
    private const string GEMS_3500 = "gems_3500";
    
    private const string BAKERY_GOLD_KEY = "bakery_gold_key";

    private const string HINTS_5 = "hints_5";
    public const string ENERGY_REFILL = "energy_refill";
    public const string ENERGY_REFILLX2 = "energy_refillx2";
    private const string COINS_1000 = "coins_1000";
    private const string COINS_5000 = "coins_5000";
    private const string COINS_10000 = "coins_10000";
    public const string REMOVEADS = "removeads";
    public const string REMOVEADS_COINS_5000 = "remove_ads_5000coins";
    
    private Boolean return_complete = true;
    private Economy economy;
    private UserSave userSave;

    private List<StoreProductCurrencyIAP> storeProducts;
    private List<StoreProductSingleItem> singleItems;
    
    public event Action<IEnumerable<PurchaseErrorCode>> OnPurchaseError;
    
    public enum PurchaseErrorCode
    {
        None = 0,
        NoOfferExisted = 1,
        UnrecognisedReward = 2,
        MultipleMatchingOffers = 3,
    }
    
    public void Initialize(Economy economy, UserSave userSave)
    {
        if (Instance != null)
        {
            throw new Exception("Duplicating IAP manager singleton");
        }

        Instance = this;

        this.economy = economy;
        this.userSave = userSave;

        if (IsInitialized())
        {
            throw new Exception("Repeat initialization of purchasing");
        }

        ConfigurationBuilder builder = ConfigurationBuilder.Instance(StandardPurchasingModule.Instance());
        
        builder.AddProduct(COINS_1000, ProductType.Consumable, new IDs {{COINS_1000, GooglePlay.Name}}, new PayoutDefinition(PayoutType.Currency, CurrencyAccount.Coins, 1000));
        builder.AddProduct(COINS_5000, ProductType.Consumable, new IDs {{COINS_5000, GooglePlay.Name}}, new PayoutDefinition(PayoutType.Currency, CurrencyAccount.Coins, 5000));
        builder.AddProduct(COINS_10000, ProductType.Consumable, new IDs {{COINS_10000, GooglePlay.Name}}, new PayoutDefinition(PayoutType.Currency, CurrencyAccount.Coins, 10000));
        
        builder.AddProduct(REMOVEADS, ProductType.Consumable, new IDs {{REMOVEADS, GooglePlay.Name}});
        builder.AddProduct(REMOVEADS_COINS_5000, ProductType.Consumable, new IDs {{REMOVEADS_COINS_5000, GooglePlay.Name}});
        
        // builder.AddProduct(GEMS_80, ProductType.Consumable, new IDs {{GEMS_80, GooglePlay.Name}}, new PayoutDefinition(PayoutType.Currency, "Gems", 80));
        // builder.AddProduct(GEMS_230, ProductType.Consumable, new IDs {{GEMS_230, GooglePlay.Name}}, new PayoutDefinition(PayoutType.Currency, "Gems", 230));
        // builder.AddProduct(GEMS_500, ProductType.Consumable, new IDs {{GEMS_500, GooglePlay.Name}}, new PayoutDefinition(PayoutType.Currency, "Gems", 500));
        // builder.AddProduct(GEMS_1100, ProductType.Consumable, new IDs {{GEMS_1100, GooglePlay.Name}}, new PayoutDefinition(PayoutType.Currency, "Gems", 1100));
        // builder.AddProduct(GEMS_2100, ProductType.Consumable, new IDs {{GEMS_2100, GooglePlay.Name}}, new PayoutDefinition(PayoutType.Currency, "Gems", 2100));
        // builder.AddProduct(GEMS_3500, ProductType.Consumable, new IDs {{GEMS_3500, GooglePlay.Name}}, new PayoutDefinition(PayoutType.Currency, "Gems", 3500));
        
        UnityPurchasing.Initialize(this, builder);
        
        storeProducts = new List<StoreProductCurrencyIAP>
        {
            new StoreProductCurrencyIAP(COINS_1000, 1000, "£1.99"),
            new StoreProductCurrencyIAP(COINS_5000, 5000, "£7.99"),
            new StoreProductCurrencyIAP(COINS_10000, 10000, "£14.99"),
            new StoreProductCurrencyIAP(REMOVEADS, 1, "£1.99"),
            new StoreProductCurrencyIAP(REMOVEADS_COINS_5000, 5000, "£3.99")
        };

        // storeProducts[0] = new StoreProductCurrencyIAP(GEMS_80, 80, "£3.59");
        // storeProducts[1] = new StoreProductCurrencyIAP(GEMS_230, 230, "£8.99");
        // storeProducts[2] = new StoreProductCurrencyIAP(GEMS_500, 500, "£17.99");
        // storeProducts[3] = new StoreProductCurrencyIAP(GEMS_1100, 1100, "£36.49");
        // storeProducts[4] = new StoreProductCurrencyIAP(GEMS_2100, 2100, "£63.99");
        // storeProducts[5] = new StoreProductCurrencyIAP(GEMS_3500, 3500, "£91.99");
        
        singleItems = new List<StoreProductSingleItem>
        {
            new StoreProductSingleItem(ENERGY_REFILL, "Refill Energy", CurrencyAccount.MaxEnergy, CurrencyAccount.Energy, 500, CurrencyAccount.Coins),
            new StoreProductSingleItem(ENERGY_REFILLX2, "Refill Energy x2", CurrencyAccount.MaxEnergy*2, CurrencyAccount.Energy, 900, CurrencyAccount.Coins)
        };
    }

    private bool IsInitialized()
    {
        return m_StoreController != null && m_StoreExtensionProvider != null;
    }

    public static IEnumerable<StoreProductCurrencyIAP> GetProducts()
    {
        return Instance.storeProducts;
    }

    public static StoreProductCurrencyIAP GetProduct(string productID)
    {
        StoreProductCurrencyIAP[] matchingProducts = Instance.storeProducts.Where(p => p.id == productID).ToArray();

        if (matchingProducts.Length != 1)
        {
            Debug.LogException(new Exception($"{matchingProducts.Length} matching products with id {productID}"));
        }
        
        return matchingProducts.FirstOrDefault();
    }
    
    public static IEnumerable<StoreProductSingleItem> GetSingleItems()
    {
        return Instance.singleItems;
    }
    
    public class StoreProductCurrencyIAP
    {
        public readonly string id;
        public readonly int amount;
        public string localisedPrice;

        public StoreProductCurrencyIAP(string id, int amount, string localisedPrice)
        {
            this.id = id;
            this.amount = amount;
            this.localisedPrice = localisedPrice;
        }
    }

    //do this once you've credited the currency
    public void CompletePurchase()
    {
        if (product == null)
        {
            Debug.LogError("Cannot complete purchase, product not initialized.");
        }
        else
        {
            m_StoreController.ConfirmPendingPurchase(product);
            Debug.Log("Completed purchase with " + product.transactionID);
        }
    }

    public void RestorePurchases()
    {
#if UNITY_IOS
        m_StoreExtensionProvider.GetExtension<IAppleExtensions>().RestoreTransactions(result => {
            if (result)
            {
                Debug.Log("Restore purchases succeeded.");
            }
            else
            {
                Debug.Log("Restore purchases failed.");
            }
        });
#endif
    }

    public void BuyProductId(string productId)
    {
        if (IsInitialized())
        {
            Product product = m_StoreController.products.WithID(productId);

            if (product != null && product.availableToPurchase)
            {
                Debug.Log(string.Format("Purchasing product:" + product.definition.id));
#if UNITY_EDITOR
                ProcessPayouts(product);
#else
                m_StoreController.InitiatePurchase(product);
#endif
            }
            else
            {
                Debug.LogError($"BuyProductID: FAIL. Not purchasing product {productId}, either is not found or is not available for purchase");
#if DEVELOPMENT
                StoreProductCurrencyIAP storeProductCurrencyIap = storeProducts.Single(p => p.id == productId);
                economy.softCurrency.Add(storeProductCurrencyIap.amount);
#endif
            }
        }
        else
        {
            Debug.LogError("BuyProductID FAIL. Not initialized.");
        }
    }

    public void OnInitialized(IStoreController controller, IExtensionProvider extensions)
    {
        Debug.Log("OnInitialized: PASS");

        m_StoreController = controller;
        m_StoreExtensionProvider = extensions;

        if (!Application.isEditor)
        {
            foreach (Product product in m_StoreController.products.all)
            {
                OverwriteLocalPrice(product);
            }
        }
    }

    private void OverwriteLocalPrice(Product product)
    {
        string id = product.definition.id;
        string localizedPrice = product.metadata.localizedPriceString;

        for (int i = 0; i < storeProducts.Count; i++)
        {
            if (storeProducts[i].id == id)
            {
                storeProducts[i].localisedPrice = localizedPrice;
                return;
            }
        }
        
        Debug.LogException(new Exception($"Couldn't find matching local product for {id}"));
    }


    public void OnInitializeFailed(InitializationFailureReason error)
    {
        // Purchasing set-up has not succeeded. Check error for reason. Consider sharing this reason with the user.
        Debug.LogError("OnInitializeFailed InitializationFailureReason:" + error);
    }
    
    
    public PurchaseProcessingResult ProcessPurchase (PurchaseEventArgs args)
    {
        bool validPurchase = true; // Presume valid for platforms with no R.V.

        // Unity IAP's validation logic is only included on these platforms.
#if UNITY_ANDROID || UNITY_IOS || UNITY_STANDALONE_OSX
        // Prepare the validator with the secrets we prepared in the Editor
        // obfuscation window.
        var validator = new CrossPlatformValidator(GooglePlayTangle.Data(),
            AppleTangle.Data(), Application.identifier);

        try {
            // On Google Play, result has a single product ID.
            // On Apple stores, receipts contain multiple products.
            var result = validator.Validate(args.purchasedProduct.receipt);
            // For informational purposes, we list the receipt(s)
            Debug.Log("Receipt is valid. Contents:");
            foreach (IPurchaseReceipt productReceipt in result) {
                Debug.Log(productReceipt.productID);
                Debug.Log(productReceipt.purchaseDate);
                Debug.Log(productReceipt.transactionID);
                
                AnalyticsProvider.Get.PurchaseVerified(args.purchasedProduct, productReceipt);
            }
        } catch (IAPSecurityException ex) {
            
            Debug.LogException(ex);
            Debug.Log("Invalid receipt, not unlocking content");
            validPurchase = false;
            AnalyticsProvider.Get.PurchaseUnverified(args.purchasedProduct);
        }
#endif

        if (validPurchase) {
            // Unlock the appropriate content here.
            Debug.Log($"ProcessPurchase: Complete. Product: {args.purchasedProduct.receipt} - {args.purchasedProduct.transactionID}");
            ProcessPayouts(args.purchasedProduct);
        }

        return PurchaseProcessingResult.Complete;
    }

    private void ProcessPayouts(Product product)
    {
        List<PurchaseErrorCode> errorCode = new List<PurchaseErrorCode>();

        string productID = product.definition?.id;
        
        Offer offer = economy.GetOffer(productID);

        if (offer != null)
        {
            economy.RedeemOffer(offer);
        }
        else
        {
            var currencyPayouts = product.definition.payouts.Where(p => p.type == PayoutType.Currency)
                .Select(p => new Offer.Reward(p.subtype, (int)p.quantity));

            Offer productOffer = OfferFromProduct(product);
            
            economy.RedeemOffer(productOffer);
        }

        decimal estimatedSpend = GetEstimatedSpend(productID);
        userSave.LogPurchase(estimatedSpend);

        if (errorCode.Count > 0)
        {
            OnPurchaseError?.Invoke(errorCode);
        }
    }

    private static Decimal GetEstimatedSpend(string productID)
    {
        switch (productID)
        {
            case COINS_1000:
            case REMOVEADS:
                return 1.99M;
            case REMOVEADS_COINS_5000:
                return 3.99M;
            case COINS_5000:
                return 7.99M;
            case COINS_10000:
                return 14.99M;
            default:
                Debug.LogError("No estimated spend defined for "+productID);
                return 0;
        }
    }

    private static Offer OfferFromProduct(Product product)
    {
        return new Offer()
        {
            ID = product.definition.id,
            Rewards = product.definition.payouts.Select(p => new Offer.Reward(p.subtype, (int)p.quantity)).ToArray()
        };
    }
    
    public void OnPurchaseFailed(Product product, PurchaseFailureReason failureReason)
    {
        Debug.LogError($"OnPurchaseFailed: FAIL. Product: '{product.definition.storeSpecificId}', PurchaseFailureReason: {failureReason}");

        AnalyticsProvider.Get.PurchaseFailed(product, failureReason);
        
        if (failureReason == PurchaseFailureReason.DuplicateTransaction)
        {
            Debug.Log("Attempting to complete the original transaction");
            CompletePurchase();
        }
    }
}