using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Firebase.Auth;
using Firebase.Database;
using Newtonsoft.Json;
using UnityEngine;

public class CloudSaveProvider : MonoBehaviour
{    
    public static CloudSaveProvider Instance { get; private set; }
    private FirebaseDatabase firebaseDB;
    
    public event Action<UserSave> CloudSaveChanged;
    
    private List<CloudSyncedObject> cloudSynced = new List<CloudSyncedObject>();
    
    public void Initialise()
    {
        Instance = this;
        
#if USE_FIREBASE_EMULATOR
        firebaseDB = FirebaseDatabase.GetInstance("http://localhost:9000/?ns=foobar");
#else
        firebaseDB = FirebaseDatabase.GetInstance("https://logic-town-default-rtdb.europe-west1.firebasedatabase.app/");
#endif
        
        Debug.Log("initialised firebase db");
    }

    private void Update()
    {
        foreach (CloudSyncedObject obj in cloudSynced)
        {
            if (obj.IsDirty)
            {
                obj.IsDirty = false;
                SaveGeneric(obj);
            }
        }
    }

    public void ClearSave()
    {
        foreach (CloudSyncedObject syncedObject in cloudSynced)
        {
            syncedObject.SetDefaults();
            syncedObject.IsDirty = true;
        }
    }

    public void StartSyncing(CloudSyncedObject syncedObject)
    {
        cloudSynced.Add(syncedObject);
    }
    
    public async Task SaveGeneric(CloudSyncedObject syncedObject)
    {
        // Get the root reference location of the database.
        DatabaseReference reference = firebaseDB.RootReference;
        string userId = FirebaseAuth.DefaultInstance.CurrentUser.UserId;
        string json = syncedObject.Serialize();

        if (string.IsNullOrEmpty(json))
        {
            throw new Exception($"Couldn't serialise {syncedObject.GetType()} as JSON");
        }
        
        Debug.Log($"Saving {syncedObject.GetType()} to cloud: {json}");
        await reference.Child(syncedObject.GetCollectionName()).Child(userId).SetRawJsonValueAsync(json);
        Debug.Log("Finished saving");
    }

    public async Task<T> LoadGeneric<T>(string collection) where T : CloudSyncedObject, new()
    {
        Debug.Log($"About to Load {typeof(T)} from cloud");
        // Get the root reference location of the database.

        if (FirebaseAuth.DefaultInstance == null || FirebaseAuth.DefaultInstance.CurrentUser == null)
        {
            throw new Exception("firebase auth has no default instance");
        }
        
        if (firebaseDB == null)
        {
            throw new Exception("firebase db has no default instance");
        }
        
        string userId = FirebaseAuth.DefaultInstance.CurrentUser.UserId;
        
        DatabaseReference reference = firebaseDB.RootReference;

        Debug.Log($"Loading {typeof(T)} from cloud: "+userId);

        var result = await reference.Child(collection).Child(userId).GetValueAsync();
        string json = result.GetRawJsonValue();

        Debug.Log(json);
        
        T save = null;
        
        if (!string.IsNullOrEmpty(json))       
            save = JsonConvert.DeserializeObject<T>(json);

        if (save == null)
        {
            Debug.Log($"Setting up default {typeof(T)}");
            save = new T();
            save.SetDefaults();
            await SaveGeneric(save);
        }
        
        StartSyncing(save);
        return save;
    }

    private async Task SaveToUserCollection(string collectionName, string json)
    {
        DatabaseReference reference = firebaseDB.RootReference;
        string userId = FirebaseAuth.DefaultInstance.CurrentUser.UserId;
        Debug.Log($"Saving to cloud {collectionName}: {json}");
        var data = await reference.Child(collectionName).Child(userId).GetValueAsync();
        Debug.Log(data.ToString());
        await reference.Child(collectionName).Child(userId).SetRawJsonValueAsync(json);
        Debug.Log($"Saved to cloud {collectionName}");
    }
    
    public void ListenForCurrencyChanges()
    {
        //pushing to realtime db is apparently free, so it might be best to use this sparingly.
        //Most transactions should work by changing the local values and pushing the changes
        //trust the client version of things for the mvp. Can go more server-authoritative if it's cost-effective. 
        string userId = FirebaseAuth.DefaultInstance.CurrentUser.UserId;
        firebaseDB.GetReference("users").Child(userId).ValueChanged += OnCurrencyChange;
        Debug.Log("Now listening for currency changes");
    }

    private void OnCurrencyChange(object sender, ValueChangedEventArgs args)
    {
        if (args.DatabaseError != null) {
            Debug.LogError(args.DatabaseError.Message);
            return;
        }

        UserSave save = JsonConvert.DeserializeObject<UserSave>(args.Snapshot.GetRawJsonValue());

        if (save == null)
        {
            Debug.LogError("save deserialization failed after a currency change");
        }
        
        //SaveGame.LoadFromCloud(save);
        CloudSaveChanged?.Invoke(save);
    }

}