public class StoreProductSingleItem
{
    public readonly string id;
    public readonly string ItemName;
    public readonly string payoutType;
    public readonly int payoutAmount;
    public readonly string costType;
    public readonly int costAmount;

    public StoreProductSingleItem(string id, string itemName, 
        int payoutAmount, string payoutType,
        int costAmount, string costType)
    {
        this.id = id;
        this.ItemName = itemName;
        this.payoutType = payoutType;
        this.payoutAmount = payoutAmount;
        this.costType = costType;
        this.costAmount = costAmount;
    }
}