using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using Framework;
using GoogleMobileAds.Api;
using Helpers;
using UI;
using UnityEngine;
using UnityEngine.Analytics;

public abstract class AdsProvider : MonoBehaviour
{
    public const string ContinuePlacement = "ContinueAd";
    public const string FreeEnergyPlacement = "FreeEnergyAd";
    public const string CoinDoublerPlacement = "CoinDoublerAd";
    public const string InterstitialPlacement = "InterstitialAd";

    protected const string TestDevicePixel3 = "947eae3a-e012-4727-bc2c-6f92fb9e0ebe";
    protected const string RewardTypeDoubleCoins = "DoubleCoins";
    protected const string RewardTypeContinue = "Continue";

    private static AdsProvider instance;
    
    protected List<Reward> pendingRewards = new List<Reward>();
    private UserSave userSave;

    protected string currentPlacement;

    public event Action OnContinueAfterLoss;
    public event Action<string,int> OnGotCurrency;
    public event Action OnDoubleCoins;
    public event Action OnInterstitialClosed;

    protected abstract AdvertisingNetwork GetNetwork();
    
    protected struct Reward
    {
        public string Type;
        public int Amount;
    }

    protected abstract void Init();
    protected abstract void Shutdown();
    protected abstract void ShowInterstitialImpl();
    protected abstract void ShowCoinDoublerImpl();
    protected abstract void ShowContinueImpl();
    protected abstract void ShowFreeEnergyImpl();
    
    public void Initialize(UserSave userSave)
    {
        if (instance != null)
            throw new Exception("Duplicate instance of singleton");

        instance = this;

        this.userSave = userSave;
        
        Init();
    }
    
    private void OnDestroy()
    {
        Shutdown();
    }

    public void ShowInterstitialAd()
    {
        instance.SetOptInThenShowAd(() =>
        {
            AnalyticsProvider.Get.StartAd(InterstitialPlacement, instance.GetNetwork(), AnalyticsProvider.AdPlacementType.Interstitial);

            if (instance.IsInterstitialAdAvailable())
            {
                instance.ShowInterstitialImpl();
            }
            else
            {
                instance.OnInterstitialClosed?.Invoke();
            }
        });
    }
        
    public void ShowCoinDoublerAd()
    {
        instance.SetOptInThenShowAd(() =>
        {
            AnalyticsProvider.Get.StartAd(CoinDoublerPlacement, instance.GetNetwork(), AnalyticsProvider.AdPlacementType.Rewarded);
            
#if UNITY_EDITOR
            instance.pendingRewards.Add(new Reward()
            {
                Type = RewardTypeDoubleCoins,
                Amount = 1
            });
#endif
            
            instance.ShowCoinDoublerImpl();
        });
    }
    
    public static void ShowContinueAd()
    {
        instance.SetOptInThenShowAd(() =>
        {
            AnalyticsProvider.Get.StartAd(ContinuePlacement, AdvertisingNetwork.Google, AnalyticsProvider.AdPlacementType.Rewarded);
            
#if UNITY_EDITOR || DEVELOPMENT
            instance.pendingRewards.Add(new Reward()
            {
                Type = RewardTypeContinue,
                Amount = 1
            });
#endif
            
            instance.ShowContinueImpl();
        });
    }

    private void SetOptInThenShowAd(Action afterOptIn)
    {
        if (HasOptInBeenSet())
        {
            afterOptIn?.Invoke();
        }
        else
        {
            var popup = UIScreen.ShowPopup<PersonalisedAdsPopup>();
            popup.OnConfirmConsent += optIn =>
            {
                OptInSetting setting = optIn ? OptInSetting.OptIn : OptInSetting.OptOut;
                PrivacyAndSettings.SetOptInSetting(setting);

                afterOptIn?.Invoke();
            };
        }
    }

    public abstract void ApplyPrivacySettings(OptInSetting optInSetting);

    private bool HasOptInBeenSet()
    {
        return PrivacyAndSettings.GetOptInSetting() != OptInSetting.NotSet;
    }

    public static void ShowFreeEnergyAd()
    {
        instance.SetOptInThenShowAd(() =>
        {
            AnalyticsProvider.Get.StartAd(FreeEnergyPlacement, instance.GetNetwork(), AnalyticsProvider.AdPlacementType.Rewarded);
            
#if UNITY_EDITOR || DEVELOPMENT
            instance.pendingRewards.Add(new Reward()
            {
                Type = CurrencyAccount.Energy,
                Amount = 50
            });
#endif
            
            instance.ShowFreeEnergyImpl();
        });
    }

    public abstract bool IsCoinDoublerAdAvailable();
    public abstract bool IsInterstitialAdAvailable();

    public abstract bool IsContinueAdAvailable();

    public abstract bool IsEnergyAdAvailable();

    public abstract void PrewarmAds();

    public virtual void PrewarmInterstitial()
    {
        
    }

    private void ProcessReward(Reward reward)
    {
        switch (reward.Type)
        {
            case CurrencyAccount.Coins:
            case CurrencyAccount.Gems:
            case CurrencyAccount.Energy:
                OnGotCurrency?.Invoke(reward.Type,reward.Amount);
                break;
            case RewardTypeContinue:
                OnContinueAfterLoss?.Invoke();
                break;
            case RewardTypeDoubleCoins:
                OnDoubleCoins?.Invoke();
                break;
            default:
                Debug.LogException(new Exception($"Unhandled ad reward {reward.Amount} {reward.Type}"));
                break;
        }
        
        userSave.WatchedRewardedAd();

        Debug.Log($"Ad reward received: {reward.Amount} {reward.Type}");
    }

    private void Update()
    {
        if (pendingRewards.Count > 0)
        {
            AnalyticsProvider.Get.RewardedAdFinished();
            currentPlacement = string.Empty;

            foreach (Reward reward in pendingRewards)
            {
                ProcessReward(reward);
            }

            pendingRewards.Clear();
        }
    }

    protected void InterstitialAdClosed()
    {
        AnalyticsProvider.Get.AdClosed();
        userSave.WatchedInterstitialAd();
        OnInterstitialClosed?.Invoke();
    }
    
    protected void InterstitialAdFailedToShow()
    {
        AnalyticsProvider.Get.LogEvent("NoInterstitialAdAvailable");
        Debug.LogError("Ad was not ready to be shown");
        OnInterstitialClosed?.Invoke();
    }
}