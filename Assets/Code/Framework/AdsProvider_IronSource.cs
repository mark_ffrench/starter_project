using System.Collections.Generic;
using System.Threading;
using Framework;
using GoogleMobileAds.Api;
using Helpers;
using UnityEngine;
using UnityEngine.Analytics;

public class AdsProvider_Ironsource : AdsProvider
{
    private const string IronSourceAppKey = "-";

    protected override AdvertisingNetwork GetNetwork()
    {
        return AdvertisingNetwork.IronSource;
    }

    protected override void Init()
    {
        IronSourceEvents.onRewardedVideoAdOpenedEvent += RewardedVideoAdOpenedEvent;
        IronSourceEvents.onRewardedVideoAdClickedEvent += RewardedVideoAdClickedEvent;
        IronSourceEvents.onRewardedVideoAdClosedEvent += RewardedVideoAdClosedEvent; 
        IronSourceEvents.onRewardedVideoAvailabilityChangedEvent += RewardedVideoAvailabilityChangedEvent;
        IronSourceEvents.onRewardedVideoAdStartedEvent += RewardedVideoAdStartedEvent;
        IronSourceEvents.onRewardedVideoAdEndedEvent += RewardedVideoAdEndedEvent;
        IronSourceEvents.onRewardedVideoAdRewardedEvent += RewardedVideoAdRewardedEvent; 
        IronSourceEvents.onRewardedVideoAdShowFailedEvent += RewardedVideoAdShowFailedEvent;
        
        IronSourceEvents.onInterstitialAdReadyEvent += InterstitialAdReadyEvent;
        IronSourceEvents.onInterstitialAdLoadFailedEvent += InterstitialAdLoadFailedEvent;        
        IronSourceEvents.onInterstitialAdShowSucceededEvent += InterstitialAdShowSucceededEvent; 
        IronSourceEvents.onInterstitialAdShowFailedEvent += InterstitialAdShowFailedEvent; 
        IronSourceEvents.onInterstitialAdClickedEvent += InterstitialAdClickedEvent;
        IronSourceEvents.onInterstitialAdOpenedEvent += InterstitialAdOpenedEvent;
        IronSourceEvents.onInterstitialAdClosedEvent += InterstitialAdClosedEvent;
        
#if DEVELOPMENT
        //enable verbose logging for ad adapters
        IronSource.Agent.setAdaptersDebug(true);
#endif
        
        IronSource.Agent.init (IronSourceAppKey, IronSourceAdUnits.REWARDED_VIDEO, IronSourceAdUnits.INTERSTITIAL);
        
        //checks if connected to internet before saying an ad is available (or something like that)
        IronSource.Agent.shouldTrackNetworkState (true);
        
        PrewarmAds();
    }
    
    void OnApplicationPause(bool isPaused) {                 
        IronSource.Agent.onApplicationPause(isPaused);
    }
    
    public override void PrewarmAds()
    {
        List<string> testDevices = new List<string>
        {
            AdRequest.TestDeviceSimulator,
            TestDevicePixel3
        };
        
        var requestConfigBuilder = new RequestConfiguration.Builder();

        if (PrivacyAndSettings.IsUserAChild())
        {
            Debug.Log("Opting out of personalised ads because the user is a child");
            
            requestConfigBuilder.SetTagForChildDirectedTreatment(TagForChildDirectedTreatment.True) //COPPA-related (13)
                .SetTagForUnderAgeOfConsent(TagForUnderAgeOfConsent.True); //GDPR-related (also 13, although higher in some countries)
                
            IronSource.Agent.setMetaData("AdMob_TFCD","true");
            IronSource.Agent.setMetaData("AdMob_TFUA","true");
        }
        else
        {
            Debug.Log("Not automatically opting out of personalised ads because of user's age");
            
            //tag for child directed treatment (TFCD) = COPPA
            //tag for under age of consent (TFUA) = GDPR
            
            requestConfigBuilder.SetTagForChildDirectedTreatment(TagForChildDirectedTreatment.False)
                .SetTagForUnderAgeOfConsent(TagForUnderAgeOfConsent.False);
            
            IronSource.Agent.setMetaData("AdMob_TFCD","false");
            IronSource.Agent.setMetaData("AdMob_TFUA","false");
        }
        
        //pretty sure this just has to match the store listing, rather than interpreting the age they give
        requestConfigBuilder.SetMaxAdContentRating(MaxAdContentRating.G)
            .SetTestDeviceIds(testDevices);
            
        RequestConfiguration configuration = requestConfigBuilder.build();
        MobileAds.SetRequestConfiguration(configuration);
    }

    public override void PrewarmInterstitial()
    {
        IronSource.Agent.loadInterstitial();
    }

    public override bool IsInterstitialAdAvailable()
    {
        bool available = IronSource.Agent.isInterstitialReady() && !IronSource.Agent.isInterstitialPlacementCapped(InterstitialPlacement);
        
        Debug.Log("InterstitialAd available "+available);
        return available;
    }

    public override bool IsContinueAdAvailable()
    {
#if UNITY_EDITOR
        return true;
#endif
        
        bool available = IronSource.Agent.isRewardedVideoAvailable() && !IronSource.Agent.isRewardedVideoPlacementCapped(ContinuePlacement);
        Debug.Log("ContinueAd available "+available);
        return available;    
    }

    public override void ApplyPrivacySettings(OptInSetting optInSetting)
    {
        if(optInSetting == OptInSetting.NotSet)
            return;

        IronSource.Agent.setConsent(optInSetting == OptInSetting.OptIn);
    }

    public override bool IsCoinDoublerAdAvailable()
    {
#if UNITY_EDITOR
        return true;
#endif
        
        bool available = IronSource.Agent.isRewardedVideoAvailable() && !IronSource.Agent.isRewardedVideoPlacementCapped(CoinDoublerPlacement);
        Debug.Log("CoinDoublerAd available "+available);
        return available;
    }
    
    public override bool IsEnergyAdAvailable()
    {
#if UNITY_EDITOR
        return true;
#endif
        
        bool available = IronSource.Agent.isRewardedVideoAvailable() && !IronSource.Agent.isRewardedVideoPlacementCapped(FreeEnergyPlacement);
        Debug.Log("FreeEnergyAd available "+available);
        return available;
    }


    protected override void Shutdown()
    {
        IronSourceEvents.onRewardedVideoAdOpenedEvent -= RewardedVideoAdOpenedEvent;
        IronSourceEvents.onRewardedVideoAdClickedEvent -= RewardedVideoAdClickedEvent;
        IronSourceEvents.onRewardedVideoAdClosedEvent -= RewardedVideoAdClosedEvent; 
        IronSourceEvents.onRewardedVideoAvailabilityChangedEvent -= RewardedVideoAvailabilityChangedEvent;
        IronSourceEvents.onRewardedVideoAdStartedEvent -= RewardedVideoAdStartedEvent;
        IronSourceEvents.onRewardedVideoAdEndedEvent -= RewardedVideoAdEndedEvent;
        IronSourceEvents.onRewardedVideoAdRewardedEvent -= RewardedVideoAdRewardedEvent; 
        IronSourceEvents.onRewardedVideoAdShowFailedEvent -= RewardedVideoAdShowFailedEvent;
        
        IronSourceEvents.onInterstitialAdReadyEvent -= InterstitialAdReadyEvent;
        IronSourceEvents.onInterstitialAdLoadFailedEvent -= InterstitialAdLoadFailedEvent;        
        IronSourceEvents.onInterstitialAdShowSucceededEvent -= InterstitialAdShowSucceededEvent; 
        IronSourceEvents.onInterstitialAdShowFailedEvent -= InterstitialAdShowFailedEvent; 
        IronSourceEvents.onInterstitialAdClickedEvent -= InterstitialAdClickedEvent;
        IronSourceEvents.onInterstitialAdOpenedEvent -= InterstitialAdOpenedEvent;
        IronSourceEvents.onInterstitialAdClosedEvent -= InterstitialAdClosedEvent;
    }

    protected override void ShowInterstitialImpl()
    {
        IronSource.Agent.showInterstitial();
    }

    protected override void ShowCoinDoublerImpl()
    {
        IronSource.Agent.showRewardedVideo(CoinDoublerPlacement);
    }

    protected override void ShowContinueImpl()
    {
        IronSource.Agent.showRewardedVideo(ContinuePlacement);
    }

    protected override void ShowFreeEnergyImpl()
    {
        IronSource.Agent.showRewardedVideo(FreeEnergyPlacement);
    }
    
    //Invoked when the Rewarded Video failed to show
    //@param description - string - contains information about the failure.
    private void RewardedVideoAdShowFailedEvent(IronSourceError error)
    {
        Debug.Log("IS RewardedVideoAdShowFailedEvent");
    }

    //Invoked when the user completed the video and should be rewarded. 
    //If using server-to-server callbacks you may ignore this events and wait for 
    // the callback from the  ironSource server.
    //@param - placement - placement object which contains the reward data
    private void RewardedVideoAdRewardedEvent(IronSourcePlacement placement)
    {
        Debug.Log("IS RewardedVideoAdRewardedEvent " + placement.getPlacementName());
        Debug.Log($"Got reward x{placement.getRewardAmount()} {placement.getRewardName()}");
        pendingRewards.Add(new Reward() {Type = placement.getRewardName(), Amount = placement.getRewardAmount()});
    }

    //Invoked when there is a change in the ad availability status.
    //@param - available - value will change to true when rewarded videos are available. 
    //You can then show the video by calling showRewardedVideo().
    //Value will change to false when no videos are available.
    private void RewardedVideoAvailabilityChangedEvent(bool available)
    {
        Debug.Log("IS RewardedVideoAvailabilityChangedEvent available: " + available);
    }

    //Invoked when the RewardedVideo ad view is about to be closed.
    //Your activity will now regain its focus.
    private void RewardedVideoAdClosedEvent()
    {
        Debug.Log("IS RewardedVideoAdClosedEvent");
    }

    //Invoked when the RewardedVideo ad view has opened.
    //Your Activity will lose focus. Please avoid performing heavy 
    //tasks till the video ad will be closed.
    private void RewardedVideoAdOpenedEvent()
    {
        Debug.Log("IS RewardedVideoAdOpenedEvent");
    }

    // ----------------------------------------------------------------------------------------
    // Note: the events below are not available for all supported rewarded video ad networks. 
    // Check which events are available per ad network you choose to include in your build. 
    // We recommend only using events which register to ALL ad networks you include in your build. 
    // ----------------------------------------------------------------------------------------

    private void RewardedVideoAdEndedEvent()
    {
        Debug.Log("IS RewardedVideoAdEndedEvent");
    }

    private void RewardedVideoAdStartedEvent()
    {
        Debug.Log("IS RewardedVideoAdStartedEvent");
    }

    private void RewardedVideoAdClickedEvent(IronSourcePlacement obj)
    {
        Debug.Log("IS RewardedVideoAdClickedEvent");
    }

    //INTERSTITIALS

    // Invoked when the initialization process has failed.
    // @param description - string - contains information about the failure.
    void InterstitialAdLoadFailedEvent(IronSourceError error)
    {
        Debug.LogError($"IS InterstitialAdLoadFailedEvent CODE={error.getCode()} ERRORCODE={error.getErrorCode()} \"{error.getDescription()}\"");
    }

    // Invoked when the ad fails to show.
    // @param description - string - contains information about the failure.
    void InterstitialAdShowFailedEvent(IronSourceError error)
    {
        Debug.LogError($"IS InterstitialAdShowFailedEvent CODE={error.getCode()} ERRORCODE={error.getErrorCode()} \"{error.getDescription()}\"");
    }

    // Invoked when end user clicked on the interstitial ad
    void InterstitialAdClickedEvent()
    {
        Debug.Log("IS InterstitialAdClickedEvent");
    }

    // Invoked when the interstitial ad closed and the user goes back to the application screen.
    void InterstitialAdClosedEvent()
    {
        Debug.Log("IS InterstitialAdClosedEvent");
        
        ThreadHelper.ExecuteOnMainThread(() =>
        {
            IronSource.Agent.loadInterstitial();
            InterstitialAdClosed();
        });
    }

    // Invoked when the Interstitial is Ready to shown after load function is called
    void InterstitialAdReadyEvent()
    {
        Debug.Log("IS InterstitialAdReadyEvent");
    }

    // Invoked when the Interstitial Ad Unit has opened
    void InterstitialAdOpenedEvent()
    {
        Debug.Log("IS InterstitialAdOpenedEvent");
    }

    // Invoked right before the Interstitial screen is about to open.
    // NOTE - This event is available only for some of the networks. 
    // You should treat this event as an interstitial impression, but rather use InterstitialAdOpenedEvent
    void InterstitialAdShowSucceededEvent()
    {
        Debug.Log("IS InterstitialAdShowSucceededEvent");
    }
}