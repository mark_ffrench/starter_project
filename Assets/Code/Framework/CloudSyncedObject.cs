using Newtonsoft.Json;
using UnityEngine;

public abstract class CloudSyncedObject
{
    private bool isDirty;
    public abstract string GetCollectionName();
    public abstract void SetDefaults();

    [JsonIgnore]
    public bool IsDirty
    {
        get => isDirty;
        set
        {
            if(value)
                Debug.Log($"Set {GetCollectionName()} dirty");
            
            isDirty = value;
        }
    }

    public virtual string Serialize()
    {
        return JsonConvert.SerializeObject(this);
    }
}