using System.Collections.Generic;
using AppsFlyerSDK;
using UnityEngine;

namespace Framework
{
    public class AttributionProvider : MonoBehaviour, IAppsFlyerConversionData
    {
        private const string AppsflyerKey = "-";

        public void Init()
        {
            AppsFlyer.setIsDebug(true);
            AppsFlyer.initSDK(AppsflyerKey, Application.identifier, this);
            AppsFlyer.startSDK();
        }

        // Mark AppsFlyer CallBacks
        public void onConversionDataSuccess(string conversionData)
        {
            AppsFlyer.AFLog("didReceiveConversionData", conversionData);
            Dictionary<string, object> conversionDataDictionary = AppsFlyer.CallbackStringToDictionary(conversionData);
            // add deferred deeplink logic here
        }

        public void onConversionDataFail(string error)
        {
            AppsFlyer.AFLog("didReceiveConversionDataWithError", error);
        }

        public void onAppOpenAttribution(string attributionData)
        {
            AppsFlyer.AFLog("onAppOpenAttribution", attributionData);
            Dictionary<string, object> attributionDataDictionary = AppsFlyer.CallbackStringToDictionary(attributionData);
            // add direct deeplink logic here
        }

        public void onAppOpenAttributionFailure(string error)
        {
            AppsFlyer.AFLog("onAppOpenAttributionFailure", error);
        }

    }
}