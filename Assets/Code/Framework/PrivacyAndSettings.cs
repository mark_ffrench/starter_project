﻿using System.Collections.Generic;
using UnityEngine;

namespace Framework
{
    public static class PrivacyAndSettings
    {
        public const string ConsentAgeKey = "consent_age";
        public const string TermsVersionConsentKey = "consent_terms_version";
        public const string PersonalisedAdsKey = "optin_personalised_ads";
        public const string NotificationsEnabledKey = "notifications_enabled";
        public const string MusicEnabledKey = "music_enabled";

        public const int TermsVersion = 1;
        private const int CoppaThresholdAge = 13;

        public static void OpenPrivacyPolicy()
        {
            string url = "https://www.markffrench.com/logic-town-privacy-policy/";
            Application.OpenURL(url);
        }

        public static void OpenTermsOfService()
        {
            string url = "https://www.markffrench.com/logic-town-terms-of-service/";
            Application.OpenURL(url);
        }

        public static void RateGame()
        {
            AnalyticsProvider.Get.LogEvent("RateGame");
#if UNITY_ANDROID
            Application.OpenURL("market://details?id="+Application.identifier);
#elif UNITY_IPHONE
            Application.OpenURL("itms-apps://itunes.apple.com/app/id"+Application.identifier);
#endif
        }

        public static bool HasSeenConsentPopup()
        {
            int versionSeen = PlayerPrefs.GetInt(TermsVersionConsentKey, 0);
            return versionSeen >= TermsVersion;
        }

        public static void ConfirmConsentAndSetAge(int age)
        {
            PlayerPrefs.SetInt(TermsVersionConsentKey, TermsVersion);
            PlayerPrefs.SetInt(ConsentAgeKey, age);
        }

        private static int GetUserAge()
        {
            int age = PlayerPrefs.GetInt(ConsentAgeKey, 0);
            return age;
        }

        public static bool IsUserAChild()
        {
            //note that age defaults to 0, so if you don't ask users to set their age they will all be children
            return GetUserAge() < CoppaThresholdAge;
        }

        public static bool HasUserOptedOutOfTracking()
        {
            //TODO: This isn't supported yet, but will need to be to support iOS ATT
            return false;
        }

        public static OptInSetting GetOptInSetting()
        {
            return (OptInSetting)PlayerPrefs.GetInt(PersonalisedAdsKey, 0);
        }

        public static void SetOptInSetting(OptInSetting setting)
        {
            PlayerPrefs.SetInt(PersonalisedAdsKey, (int)setting);
            
            Dictionary<string, object> payload = new Dictionary<string, object>();
            payload.Add("OptIn", setting);
            AnalyticsProvider.Get.LogEvent("PersonalisedAdsSet", payload);
        }
        
        public static bool GetNotificationsSetting()
        {
            return PlayerPrefs.GetInt(NotificationsEnabledKey, 1) == 1;
        }
        
        public static void SetNotificationsSetting(bool setting)
        {
            PlayerPrefs.SetInt(NotificationsEnabledKey, setting ? 1 : 0);
            
            Dictionary<string, object> payload = new Dictionary<string, object>();
            payload.Add("Enabled", setting);
            AnalyticsProvider.Get.LogEvent("NotificationsToggled", payload);
        }
        
        public static bool GetMusicSetting()
        {
            return PlayerPrefs.GetInt(MusicEnabledKey, 1) == 1;
        }
        
        public static void SetMusicSetting(bool setting)
        {
            PlayerPrefs.SetInt(MusicEnabledKey, setting ? 1 : 0);
            
            Dictionary<string, object> payload = new Dictionary<string, object>();
            payload.Add("Enabled", setting);
            AnalyticsProvider.Get.LogEvent("MusicToggled", payload);
        }
    }
}