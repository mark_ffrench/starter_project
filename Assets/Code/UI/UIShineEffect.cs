﻿using System;
using Helpers;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    [RequireComponent(typeof(Image))]
    public class UIShineEffect : MonoBehaviour
    {
        [SerializeField] private Image image;

        public float duration = 1f;
        public float interval = 3f;

        private Material material;
        private float shineWidth;
        private float timeOffset = 0f;
        
        private static readonly int ShineLocation = Shader.PropertyToID("_ShineLocation");
        private static readonly int ShineColor = Shader.PropertyToID("_ShineColor");
        private static readonly int ShineWidth = Shader.PropertyToID("_ShineWidth");

        private void Awake()
        {
            if (image == null)
            {
                Debug.LogError($"UIShineEffect had to grab image at runtime {name}");
                image = GetComponent<Image>();
            }
            
            if (image.material.shader.name != "UI/ShineEffect")
            {
                Debug.LogError($"Incorrect material on {name}: {image.material.shader.name}");
            }
            else
            {
                material = Instantiate(image.material);
                image.material = material;
                shineWidth = image.material.GetFloat(ShineWidth);
            }
        }

        private void OnDisable()
        {
            material.SetColor(ShineColor, Color.clear);
        }

        private void Update()
        {
            float time = Time.realtimeSinceStartup + timeOffset;
            float loopTime = duration + interval;

            float t = time % loopTime;

            if (t > interval)
            {
                t -= interval;
                t /= duration;
                
                t = Easing.Quadratic.InOut(t);
                
                material.SetFloat(ShineLocation, t);

                float sin0to1 = Mathf.Sin(t * Mathf.PI);

                material.SetFloat(ShineWidth, shineWidth * sin0to1);
                material.SetColor(ShineColor, Color.white);
            }
            else
            {
                material.SetColor(ShineColor, Color.clear);
            }
        }

        public void PlayOnce()
        {
            enabled = true;
            
            //ensure that the shine starts immediately, since it does a modulo of realTimeSinceStartup in the Update loop
            float loopTime = duration + interval;
            timeOffset = interval - (Time.realtimeSinceStartup % loopTime);
            
            //Debug.Log($"{loopTime} - ({Time.realtimeSinceStartup} % {loopTime}) - {interval} = {timeOffset}");
            
            //Invoke(nameof(DelayedDisable), duration);
        }

        private void DelayedDisable()
        {
            enabled = false;
        }
    }
}