using UnityEngine;

namespace UI
{
    public class UIOverlay : UIScreen
    {
        public void showIfHidden()
        {
            if (gameObject.activeSelf)
            {
                return;
            }

            gameObject.SetActive(true);
            OnShow();
        }
    }
}