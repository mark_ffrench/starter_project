﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class FillBar : MonoBehaviour
{
    [SerializeField] private Image fill;
    [SerializeField] private TextMeshProUGUI label;
    
    public float Value
    {
        set
        {
            value = Mathf.Clamp01(value);
            fill.fillAmount = value;
        }
    }

    public void SetWithInts(int val, int max)
    {
        Value = (float) val / max;
    }

    public string Label
    {
        set { label.text = value; }
    }

    private void Awake()
    {
        if (fill == null)
        {
            throw new Exception("Fill image not set up on "+gameObject);
        }
        //Value = 0f;
    }
}
