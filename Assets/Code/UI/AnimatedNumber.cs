using System;
using System.Collections;
using TMPro;
using UnityEngine;

[ExecuteInEditMode]
public class AnimatedNumber : MonoBehaviour
{
    public int m_startValue;
    public int m_endValue;

    [Tooltip("Use this slider to update the value using an Animator")]
    [Range(0,1f)]
    [SerializeField] private float m_lerp;
	
    private TextMeshProUGUI m_label;

    public string m_format = "{0}";

    private const float m_pulseFrequency = 0.2f;

    //this animates the with the same scaling pulse as the text
    [SerializeField] private RectTransform m_linkedImage;
    
    private void Awake()
    {
        m_label = GetComponent<TextMeshProUGUI>();
        m_label.text = "";
    }

    private void OnDisable()
    {
        StopAllCoroutines();
    }

    /// <summary>
    /// Trigger an animation coroutine via script
    /// </summary>
    /// <param name="delay"></param>
    /// <param name="duration"></param>
    public void Animate(float delay, float duration)
    {
        StartCoroutine(doCountdown(delay, duration));
    }
	
    private IEnumerator doCountdown(float m_delay, float m_duration)
    {
        int value = m_startValue;
        //m_label.text = string.Format(m_format, value);
        m_lerp = 0;

        yield return new WaitForSecondsRealtime(m_delay);

        float scalePulseTimer = 0;
        
        Vector3 startScale = m_label.rectTransform.localScale;
        
        while (value != m_endValue)
        {
            scalePulseTimer += Time.unscaledDeltaTime;
            m_lerp += Time.unscaledDeltaTime / m_duration;
            value = (int) Mathf.Lerp(m_startValue, m_endValue, m_lerp);
            //m_label.text = string.Format(m_format, value);

            m_label.rectTransform.localScale = startScale * (1f + scalePulseTimer % m_pulseFrequency);

            if (m_linkedImage != null)
            {
                m_linkedImage.localScale = Vector3.one * (1f + scalePulseTimer % m_pulseFrequency);
            }
            
            yield return new WaitForEndOfFrame();
        }

        m_label.rectTransform.localScale = startScale;
    }

    private void Update()
    {
        int value = (int) Mathf.Lerp(m_startValue, m_endValue, m_lerp);
        m_label.text = string.Format(m_format, value);
    }

    public void Reset()
    {
        m_lerp = 0f;
    }
}
