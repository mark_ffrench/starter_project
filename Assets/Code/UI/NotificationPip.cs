using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class NotificationPip : MonoBehaviour
    {
        [SerializeField] private Image image;
        [SerializeField] private TextMeshProUGUI label;

        public void Set(int count)
        {
            image.enabled = count > 0;
            label.enabled = count > 0;

            if (count > 9)
            {
                label.text = "9+";
            }
            else
            {
                label.text = count.ToString();
            }
        }

        public void Set(bool enabled)
        {
            image.enabled = enabled;
            label.enabled = false;
        }

    }
}