using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Framework;
using Helpers;
using JetBrains.Annotations;
using UnityEngine;

namespace UI
{
    public abstract class UIScreen : MonoBehaviour
    {
        private static readonly Dictionary<Type, GameObject> prefabs = new Dictionary<Type, GameObject>();
        private static readonly Dictionary<Type, UIScreen> screens = new Dictionary<Type, UIScreen>();
        private static readonly Dictionary<Type, UIOverlay> overlays = new Dictionary<Type, UIOverlay>();
        private static readonly Dictionary<Type, UIPopup> popups = new Dictionary<Type, UIPopup>();

        private static Transform parent;
        private static UIScreen currentScreen;
        private static UIPopup currentPopup;
        
        [SerializeField] private UIOverlay[] overlaysToShow;
        [SerializeField] private bool allowTouchInput;
        
        private static UIScreen previousScreen;

        private CanvasGroup canvasGroup;
        protected Canvas canvas;
        protected Animator animator;

        private float transitionInTime;
        private float transitionOutTime;
        
        private void Awake ()
        {
            canvas = GetComponent<Canvas>();
            canvas.renderMode = RenderMode.ScreenSpaceCamera;
            canvas.worldCamera = Camera.main;

            canvasGroup = GetComponent<CanvasGroup>();

            animator = GetComponent<Animator>();

            if (animator != null)
            {
                animator.keepAnimatorControllerStateOnDisable = true;
                animator.cullingMode = AnimatorCullingMode.AlwaysAnimate;

                foreach (AnimationClip clip in animator.runtimeAnimatorController.animationClips)
                {
                    if (clip.name.EndsWith("TransitionIn"))
                    {
                        transitionInTime = clip.length;
                        //Debug.LogError($"Found clip {clip.name} transition in time = {transitionInTime}");
                    }
                    if (clip.name.EndsWith("TransitionOut"))
                    {
                        transitionOutTime = clip.length;
                    }
                }    
                
                //Debug.LogError($"{this.gameObject.name} transition in time = {transitionInTime}");
            }
        }

        private float refreshTimer;
        
        private void Update()
        {
            refreshTimer -= Time.deltaTime;

            if (refreshTimer <= 0)
            {
                refreshTimer = 1f;
                EverySecond();
            }
        }

        protected virtual void EverySecond()
        {
            
        }
        
        public static void AddPrefab<T>(T screen) where T : UIScreen
        {
            if(screen == null)
                throw new ArgumentNullException(nameof(screen), "No prefab assigned for "+typeof(T));
            
            prefabs.Add(typeof(T), screen.gameObject);
        }
        
        public static void AddOverlayPrefab<T>(T screen) where T : UIOverlay
        {
            if(screen == null)
                throw new ArgumentNullException(nameof(screen), "No prefab assigned for "+typeof(T));
            
            prefabs.Add(typeof(T), screen.gameObject);
        }

        protected virtual void HandlePayload(object obj)
        {
            
        }

        protected void DoAnimationThenSleep(string trigger, float sleepDelay = 1f)
        {
            Debug.Log("DoAnim - "+trigger);
            StartCoroutine(triggerAnimThenSleepCoroutine(trigger, sleepDelay));
        }

        private IEnumerator triggerAnimThenSleepCoroutine(string trigger, float delay)
        {
            if (animator == null) 
                throw new NullReferenceException("No animator on "+this);
            
            animator.enabled = true;
            animator.SetTrigger(trigger);
            
            yield return new WaitForSeconds(delay);

            //ffwd anim to end
            animator.speed = float.MaxValue;
            yield return new WaitForEndOfFrame();

            animator.ResetTrigger(trigger);
            
            animator.speed = 1f;
            animator.enabled = false;
        }

        private void TransitionIn()
        {
            if (animator != null)
            {
                canvasGroup.blocksRaycasts = false;

                animator.enabled = true;
                animator.SetTrigger("TransitionIn");
                
                Invoke(nameof(FinishedTransitionIn), transitionInTime);
            }
            else
            {
                canvasGroup.blocksRaycasts = true;
            }
        }
        
        private void TransitionOut()
        {
            canvasGroup.blocksRaycasts = false;
            
            if (animator != null)
            {
                animator.enabled = true;
                animator.SetTrigger("TransitionOut");
                
                Invoke(nameof(FinishedTransitionOut), transitionOutTime);
            }
            else
            {
                OnHide();
                gameObject.SetActive(false);
            }
        }

        private void FinishedTransitionIn()
        {
            animator.speed = float.MaxValue;
            Invoke(nameof(CleanupTransitionIn), 0.1f);
        }
        
        private void FinishedTransitionOut()
        {
            animator.speed = float.MaxValue;
            Invoke(nameof(CleanupTransitionOut), 0.1f);
        }

        private void CleanupTransitionIn()
        {
            animator.ResetTrigger("TransitionIn");
            animator.speed = 1f;
            animator.enabled = false;
            canvasGroup.blocksRaycasts = true;
            
            AfterTransitionedIn();
        }

        protected virtual void AfterTransitionedIn()
        {
            
        }
        
        private void CleanupTransitionOut()
        {
            animator.ResetTrigger("TransitionOut");
            animator.speed = 1f;
            animator.enabled = false;
            
            OnHide();
            gameObject.SetActive(false);
        }

        protected virtual void OnHide()
        {
            
        }

        protected virtual void OnShow()
        {
            
        }

        [CanBeNull]
        public static T GetScreenIfActive<T>() where T : UIScreen
        {
            if (currentScreen is T screen)
            {
                return screen;
            }

            return null;
        }

        public static UIScreen Back()
        {
            if (previousScreen == null)
            {
                Debug.LogError($"Can't go back from {currentScreen}");
                return currentScreen;
            }
            
            hideScreen(currentScreen);
            
            currentScreen = previousScreen;
            currentScreen.showIfHidden();
            currentScreen.OnShow();
            currentScreen.ShowOverlays();
            currentScreen.TransitionIn();
            
            SetGameInputEnabled(currentScreen.allowTouchInput);

            return currentScreen;
        }
        
        public static T GoTo<T>(object payload = null) where T : UIScreen
        {           
            if (currentScreen != null)
            {
                if (currentScreen is T alreadyShowing)
                    return alreadyShowing;
                
                hideScreen(currentScreen);
                previousScreen = currentScreen;
                currentScreen = null;
            }
            
            Type t = typeof(T);
            
            //already spawned
            if (screens.TryGetValue(t, out currentScreen))
            {
                currentScreen.showIfHidden();
            }
            else
            {
                currentScreen = spawnPrefab<T>(t);
            }
            
            currentScreen.HandlePayload(payload);
            currentScreen.OnShow();
            currentScreen.ShowOverlays();
            currentScreen.TransitionIn();
            
            SetGameInputEnabled(currentScreen.allowTouchInput);

            AnalyticsProvider.Get.GoToScreen<T>();
            
            return (T)currentScreen;
        }

        public static bool IsPopupShowing<T>() where T : UIPopup
        {
            Type t = typeof(T);
            
            //already spawned
            if (popups.TryGetValue(t, out UIPopup popup))
            {
                return popup.gameObject.activeSelf;
            }

            return false;
        }

        public static T GetPopupIfActive<T>() where T : UIPopup
        {
            Type t = typeof(T);
            
            //already spawned
            if (popups.TryGetValue(t, out UIPopup popup))
            {
                if (popup.gameObject.activeSelf)
                {
                    return (T)popup;
                }
            }

            return null;
        }
        
        public static T ShowPopup<T>(object payload = null) where T : UIPopup
        {
            Type t = typeof(T);

            int sortOrder = -1;
            
            if (currentPopup != null)
            {
                sortOrder = currentPopup.canvas.sortingOrder;
            }
            
            //already spawned
            if (popups.TryGetValue(t, out UIPopup screen))
            {
                screen.showIfHidden();
            }
            else
            {
                screen = spawnPrefab<T>(t) as UIPopup;
                popups.Add(t, screen);
            }

            //make sure this popup displays above any previous popup
            if (sortOrder > -1 && screen.canvas.sortingOrder <= sortOrder)
            {
                screen.canvas.sortingOrder = sortOrder + 1;
            }
            
            screen.HandlePayload(payload);
            screen.OnShow();
            screen.TransitionIn();

            currentPopup = screen;
            
            //assume popups do not allow input
            SetGameInputEnabled(false);
            
            AnalyticsProvider.Get.ShowPopup<T>();

            return (T)screen;
        }
        
        protected void ShowOverlays()
        {            
            var overlaysToHide = overlays.Keys.Except(overlaysToShow.Select(o => o.GetType()));
            
            //hide old overlays
            foreach (Type t in overlaysToHide)
            {
                var o = overlays[t];
                
                if (!o.gameObject.activeSelf)
                {
                    continue;
                }

                o.OnHide();
                o.gameObject.SetActive(false);
            }
            
            foreach (UIOverlay overlayPrefab in overlaysToShow)
            {
                Type t = overlayPrefab.GetType();

                if (overlays.TryGetValue(t, out UIOverlay overlayInstance))
                {
                    overlayInstance.showIfHidden();
                    overlayInstance.OnShow();
                }
                else
                {
                    GameObject go = Instantiate(overlayPrefab.gameObject, parent);
                    overlayInstance = go.GetComponent(t) as UIOverlay;

                    if (overlayInstance == null)
                    {
                        throw new Exception($"No {t} found on prefab {go}");
                    }

                    DependencyResolver.InjectDependencies(overlayInstance);

                    overlays.Add(t, overlayInstance);
                    overlayInstance.OnShow();
                }
                
                overlayInstance.TransitionIn();
                overlayInstance.canvasGroup.blocksRaycasts = true;
            }
        }

        private void showIfHidden() 
        {
            if (gameObject.activeSelf)
            {
                return;
            }

            gameObject.SetActive(true);
        }

        private static UIScreen spawnPrefab<T>(Type t) where T : UIScreen
        {
            if (parent == null)
            {
                GameObject o = new GameObject();
                o.name = "UI";
                parent = o.transform;
                parent.SetPositionAndRotation(Vector3.zero, Quaternion.identity);
            }
            
            UIScreen screen;
            if (!prefabs.TryGetValue(t, out GameObject prefab))
            {
                throw new Exception($"No prefab defined for {typeof(T)}");
            }

            GameObject go = Instantiate(prefab, parent);
            screen = go.GetComponent<T>();

            if (screen == null)
            {
                throw new Exception($"No {typeof(T)} found on prefab {go}");
            }

            DependencyResolver.InjectDependencies(screen);

            screens.Add(t, screen);

            return screen;
        }

        public static void Hide<T>() where T : UIScreen
        {
            if (screens.TryGetValue(typeof(T), out UIScreen screen))
            {
                if (!screen.gameObject.activeSelf)
                {
                    return;
                }
                
                hideScreen(screen);

                if (screen is UIPopup)
                {
                    if (currentScreen != null)
                    {
                        SetGameInputEnabled(currentScreen.allowTouchInput);
                    }
                }
            }
            else
            {
                Debug.Log($"Screen {typeof(T)} has not been spawned so can not be turned off'");
            }
        }
        
        public static void HideOverlay<T>() where T : UIOverlay
        {
            if (overlays.TryGetValue(typeof(T), out UIOverlay overlay))
            {
                if (!overlay.gameObject.activeSelf)
                {
                    return;
                }
                
                hideScreen(overlay);
            }
            else
            {
                Debug.Log($"Overlay {typeof(T)} has not been spawned so can not be turned off'");
            }
        }

        private static void hideScreen(UIScreen screen)
        {
            screen.TransitionOut();
        }

        private static void SetGameInputEnabled(bool allowTouchInput)
        {
            //enable/disable game input if there is a popup up
        }
    }
}