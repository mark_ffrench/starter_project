using System;
using System.Collections.Generic;
using System.Linq;
using Framework;
using Helpers;
using UnityEngine;

public class Economy
{
    public CurrencyAccount hardCurrency;
    public CurrencyAccount softCurrency;
    public CurrencyAccount energy;
    public CurrencyAccount xp;

    public int Level => save.currentLevel;

    private const string PersistentReward_RemoveAds = "RemoveAds";
    private const int skipSecondsPerGem = 600;
    private const int relicPointsPerCommonCard = 120;
    
    public const string SourceBoosterPopup = "BoosterPopup";
    public const string SourceStorePurchase = "StorePurchase";
    public const string SourceWatchedAd = "WatchedAd";
    public const string SourceCoinDoubler = "CoinDoubler";
    public const string SourcePuzzle = "PuzzleReward";
    public const string SourceLeaderboard = "DailyPuzzleLeaderboard";

    private UserSave save;
    private List<Offer> OfferDefinitions = new List<Offer>();

    public static readonly int[] XpLevels = {
        100, 150, 220, 350, 500, 750, 1000, 1200, 1500, 2000, 3000, 5000, 7000, 10000, 13000, 16000, 20000, 24000, 28000, 33000, 38000, 43000
    };
    
    public event Action<Offer> OnOfferPurchased;
    //todo: save these in cloud
    public DateTime NextEnergyTime { get; private set; }
    public DateTime EnergyFullTime { get; private set; }
    
    public Economy(UserSave save)
    {
        this.save = save;
            
        softCurrency = new CurrencyAccount(CurrencyAccount.Coins, save);
        hardCurrency = new CurrencyAccount(CurrencyAccount.Gems, save);
        energy = new CurrencyAccount(CurrencyAccount.Energy, save);
        xp = new CurrencyAccount(CurrencyAccount.XP, save);
            
        OfferDefinitions.Clear();
        OfferDefinitions.Add(new Offer
        {
            ID = IAPProvider.REMOVEADS, Rewards = new []
            {
                new Offer.Reward(Economy.PersistentReward_RemoveAds, 1)
            }
        });
        
        OfferDefinitions.Add(new Offer
        {
            ID = IAPProvider.REMOVEADS_COINS_5000, 
            Rewards = new []
            {
                new Offer.Reward(Economy.PersistentReward_RemoveAds, 1),
                new Offer.Reward(CurrencyAccount.Coins, 5000),
            }
        });
        
#if ENERGY_MECHANIC
        ClaimEnergy();
#endif
        
        CloudSaveProvider.Instance.CloudSaveChanged += OnCloudSaveChanged;
    }

    public void OnCloudSaveChanged(UserSave save)
    {
        softCurrency.Set(save.numCoins);
        hardCurrency.Set(save.numGems);
    }

    public static int GetSkipCost(int seconds)
    {
        return Mathf.CeilToInt((float)seconds / skipSecondsPerGem);
    }

    public void MakePurchase(string itemId)
    {
        var singleItems = IAPProvider.GetSingleItems();

        foreach (StoreProductSingleItem item in singleItems)
        {
            if (item.id == itemId)
            {
                MakePurchase(item);
                return;
            }
        }
    }

    public void MakePurchase(StoreProductSingleItem item)
    {
        CurrencyAccount payingAccount = GetAccountForIAPType(item.costType);
        CurrencyAccount receivingAccount = GetAccountForIAPType(item.payoutType);
            
        if (payingAccount.TrySubtract(item.costAmount, item.id))
        {
            receivingAccount.Add(item.payoutAmount, SourceStorePurchase);
            AnalyticsProvider.Get.PurchaseItemWithCurrency(item);
        }
    }

    public void MakePurchase(string itemType, int amount)
    {
        var item = IAPProvider.GetSingleItems().Single(i => i.payoutType == itemType && i.payoutAmount == amount);
            
        MakePurchase(item);
    }

    private CurrencyAccount GetAccountForIAPType(string type)
    {
        switch (type)
        {
            case CurrencyAccount.Gems:
                return hardCurrency;
            case CurrencyAccount.Coins:
                return softCurrency;
            case CurrencyAccount.Energy:
                return energy;
            default:
                throw new ArgumentException("No currency found of type "+type);
        }
    }

    public bool TrySpend(string type, int amount, string analyticsName)
    {
        return GetAccount(type).TrySubtract(amount, analyticsName);
    }
        
    public int GetBalance(string type)
    {
        return GetAccount(type).Amount;
    }

    public CurrencyAccount GetAccount(string type)
    {
        switch (type)
        {
            case CurrencyAccount.Coins:
                return softCurrency;
            case CurrencyAccount.Gems:
                return hardCurrency;
            case CurrencyAccount.Energy:
                return energy;
            default:
                throw new ArgumentOutOfRangeException();
        }
    }

    public bool TrySpendEnergy(int amount, string itemName = null)
    {
        if (GetAccount(CurrencyAccount.Energy).TrySubtract(amount, itemName))
        {
            SetEnergyTimeStamp();
            return true;
        }

        return false;
    }

    private void SetEnergyTimeStamp()
    {
        DateTime claimTime = DateTimeProvider.Now;
        save.energyTouchedAt = claimTime.Ticks;
        NextEnergyTime = claimTime + TimeSpan.FromSeconds(CurrencyAccount.SecondsPerEnergy);
        int remainingEnergy = CurrencyAccount.MaxEnergy - energy.Amount;
        EnergyFullTime = claimTime + TimeSpan.FromSeconds(CurrencyAccount.SecondsPerEnergy * remainingEnergy);
    }

    public void ClaimEnergy()
    {
        if (energy.Amount >= CurrencyAccount.MaxEnergy)
        {
            return;
        }

        DateTime lastEnergyClaimTime = new DateTime(save.energyTouchedAt);
        TimeSpan elapsed = DateTimeProvider.Now - lastEnergyClaimTime;

        int amt = (int) Math.Min(Math.Floor(elapsed.TotalSeconds / CurrencyAccount.SecondsPerEnergy), CurrencyAccount.MaxEnergy);
        amt = Math.Max(amt, 0);
            
        energy.AddUpToLimit(amt, CurrencyAccount.MaxEnergy, "EnergyTimer");

        SetEnergyTimeStamp();
    }

    public void RefillEnergy()
    {
        if (energy.Amount >= CurrencyAccount.MaxEnergy)
        {
            return;
        }
            
        energy.Set(CurrencyAccount.MaxEnergy);
        SetEnergyTimeStamp();
    }
    
    public int GetXPToNextLevel()
    {
        if(Level <= 0 || Level >= XpLevels.Length)
            throw new ArgumentOutOfRangeException();
            
        return XpLevels[Level-1];
    }
        
    public void AddXP(int numXp)
    {
        xp.Add(numXp);
    }

    public bool CanLevelUp()
    {
        return xp.Amount >= GetXPToNextLevel();
    }

    public void LevelUp()
    {
        if(Level >= XpLevels.Length)
            throw new Exception("Player has reached max level");
            
        if (xp.TrySubtract(GetXPToNextLevel()))
        {
            save.currentLevel++;
        }
            
#if ENERGY_MECHANIC
            RefillEnergy();
#endif
        Debug.Log($"Levelled up to {Level}");
    }

    public void RedeemOffer(Offer offer)
    {
        Debug.Log("Redeeming offer "+offer.ID);

        foreach (Offer.Reward reward in offer.Rewards)
        {
            if(reward.Type == PersistentReward_RemoveAds)
            {
                AnalyticsProvider.Get.LogEvent("RemovedAds");
                save.AdsRemoved = true;
                save.IsDirty = true;
                AnalyticsProvider.Get.OnUserUpdated(save);
            }
            else
            {
                try
                {
                    CurrencyAccount account = GetAccount(reward.Type);
                    account.Add(reward.Amount, SourceStorePurchase);
                }
                catch (ArgumentOutOfRangeException ex)
                {
                    Debug.LogException(ex);
                }
            }
        }
        
        OnOfferPurchased?.Invoke(offer);
    }

    public Offer GetOffer(string productID)
    {
        if (!string.IsNullOrEmpty(productID))
        {
            Offer[] matchingOffers = OfferDefinitions.Where(o => o.ID == productID).ToArray();

            if (matchingOffers.Length == 0)
            {
                return null;
            }
            
            if (matchingOffers.Length > 1)
            {
                Debug.LogException(new Exception($"Client contains {matchingOffers.Length} offers with the ID {productID}"));
            }

            return matchingOffers[0];
        }

        Debug.LogException(new Exception("Tried to get a offer with a null or empty productID"));
        return null;
    }
}