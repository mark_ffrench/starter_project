using System;

public class Offer
{
    public string ID;
    public Reward[] Rewards = Array.Empty<Reward>();
    public int MaxPurchaseCount = 1;
    public DateTime ExpiryDate { get; set; }

    public struct Reward
    {
        public string Type;
        public int Amount;

        public Reward(string type, int amount)
        {
            Type = type;
            Amount = amount;
        }
    }
}