﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Framework;
using Helpers;
using UI;
#if UNITY_ANDROID
using Unity.Notifications.Android;
#endif
using UnityEngine;

public class BootLoader : MonoBehaviour
{
    private CrashlyticsProvider crashlyticsProvider;
    private AuthProvider authProvider;
    private CloudSaveProvider cloudSaveProvider;
    private UserSave userSave;
    private Economy economy;
    private BootScreen screen;
    private TermsOfServicePopup termsPopup;
    private LoginResult lastLoginResult;

    //todo: stop maintaining this list of screens manually
    [SerializeField] private HomeScreen homeScreen = null;
    [SerializeField] private NegativeFeedbackPopup negativeFeedbackPopup = null;
    [SerializeField] private SettingsPopup settingsPopup = null;
    [SerializeField] private NoInternetPopup noInternetPopup = null;
    [SerializeField] private RateTheGamePopup rateTheGamePopup = null;

    [SerializeField] protected BootScreen bootScreenPrefab = null;
    [SerializeField] protected TermsOfServicePopup termsOfServicePopup = null;
    [SerializeField] protected PersonalisedAdsPopup personalisedAdsPopup = null;

    private async Task PreAuthBoot()
    {
        await LoadCrashlytics();
    }

    private async Task PostAuthBoot()
    {
        cloudSaveProvider = GameObjectFactory.Create<CloudSaveProvider>();

        //Fetch the current DateTime from Firebase, this acts as a login check to see if we're connected.
        lastLoginResult = await DateTimeProvider.WriteLoginTimestamp();

        Debug.Log("Got login result " + lastLoginResult.Success + " Elapsed: " + lastLoginResult.TimeSinceLastSession);
        
        if (!lastLoginResult.Success)
        {
            Debug.LogError("No internet");
            UIScreen.ShowPopup<NoInternetPopup>();

            //try again
            lastLoginResult = await DateTimeProvider.WriteLoginTimestamp();

            if (!lastLoginResult.Success)
            {
                return;
            }

            UIScreen.Hide<NoInternetPopup>();
        }

        userSave = await LoadCloudSave();
        DependencyResolver.AddDependency(userSave);

        userSave.RecordNewSession(this);

        Localisation localisation = new Localisation();
        localisation.LoadLanguage(LanguageCode.EN);
        DependencyResolver.AddDependency(localisation);

        economy = new Economy(userSave);
        DependencyResolver.AddDependency(economy);

        SetupNotificationChannels();

        Debug.Log("Loading ads provider");
        AdsProvider adsProvider = GameObjectFactory.Create<AdsProvider_Ironsource>();
        adsProvider.Initialize(userSave);
        DependencyResolver.AddDependency<AdsProvider>(adsProvider);

        Debug.Log("Loading iap provider");
        IAPProvider iapProvider = GameObjectFactory.Create<IAPProvider>();
        iapProvider.Initialize(economy, userSave);

        UIScreen.AddPrefab(homeScreen);
        UIScreen.AddPrefab(settingsPopup);
        UIScreen.AddPrefab(rateTheGamePopup);
        UIScreen.AddPrefab(negativeFeedbackPopup);
        UIScreen.AddPrefab(personalisedAdsPopup);
        UIScreen.AddPrefab(termsOfServicePopup);

        AnalyticsProvider.Get.OnUserUpdated(userSave);
        CheckIfOpenedFromNotification();

        AnalyticsProvider.Get.OnBootFinished(Time.realtimeSinceStartup, lastLoginResult);
    }

    private void OnBootFinished()
    {
        Debug.Log("Boot finished");
        UIScreen.GoTo<HomeScreen>();
    }

    private async Task LoadCrashlytics()
    {
#if UNITY_ANDROID || UNITY_IPHONE
        Debug.Log("Loading crashlytics");

        //should wait for this to finish initialising, so it catches startup errors
        crashlyticsProvider = GameObjectFactory.Create<CrashlyticsProvider>();
        await crashlyticsProvider.Initialize();
#endif
    }

    private async Task<UserSave> LoadCloudSave()
    {
        UserSave save = await cloudSaveProvider.LoadGeneric<UserSave>(UserSave.CollectionName);
        cloudSaveProvider.ListenForCurrencyChanges();

        if (AuthProvider.User != null)
        {
            save.name = AuthProvider.User.DisplayName;
            save.socialID = AuthProvider.User.ProviderId;
        }

        return save;
    }

    private void Start()
    {
        //need these screens before boot
        UIScreen.AddPrefab(bootScreenPrefab);
        UIScreen.AddPrefab(termsOfServicePopup);
        UIScreen.AddPrefab(noInternetPopup);

        screen = UIScreen.GoTo<BootScreen>();
        screen.OnContinueBoot += ContinueBoot;

        if (PrivacyAndSettings.HasSeenConsentPopup())
        {
            screen.AfterConsent();
        }
        else
        {
            Invoke(nameof(ShowConsentPopup), 1f);
        }

        Application.targetFrameRate = 60;
        Screen.sleepTimeout = SleepTimeout.NeverSleep;
    }

    private void ShowConsentPopup()
    {
        termsPopup = UIScreen.ShowPopup<TermsOfServicePopup>();
        termsPopup.OnConfirmConsentWithAge += OnConfirmConsentWithAge;
    }

    private async void OnConfirmConsentWithAge(int age)
    {
        PrivacyAndSettings.ConfirmConsentAndSetAge(age);
        termsPopup.OnConfirmConsentWithAge -= OnConfirmConsentWithAge;

        try
        {
            await PreAuthBoot();
        }
        catch (Exception ex)
        {
            Debug.LogException(ex);
        }

        screen.AfterConsent();
    }

    private async void ContinueBoot()
    {
        screen.OnContinueBoot -= ContinueBoot;

        try
        {
            await PostAuthBoot();
        }
        catch (Exception ex)
        {
            Debug.LogException(ex);
            throw;
        }

        Debug.Log("Boot finished");
        OnBootFinished();
    }


    //TODO: Move all this notification stuff out into a separate class
    private void OnApplicationPause(bool pauseStatus)
    {
        if (pauseStatus && PrivacyAndSettings.GetNotificationsSetting())
        {
            ScheduleLocalNotifications();
        }

        if (!pauseStatus)
        {
            CheckIfOpenedFromNotification();
        }
    }

    private static void SetupNotificationChannels()
    {
#if UNITY_ANDROID
        AndroidNotificationChannel c = new AndroidNotificationChannel()
        {
            Id = "timers",
            Name = "Reminders",
            Importance = Importance.High,
            Description = "Lets you know when new puzzles are available, or rewards are ready",
        };
        AndroidNotificationCenter.RegisterNotificationChannel(c);
#endif

        Debug.Log("Registered notification channels");
    }
    
    private static void CheckIfOpenedFromNotification()
    {
#if UNITY_ANDROID
        AndroidNotificationIntentData notificationIntentData = AndroidNotificationCenter.GetLastNotificationIntent();

        if (notificationIntentData != null)
        {
            int id = notificationIntentData.Id;
            string channel = notificationIntentData.Channel;
            AndroidNotification notification = notificationIntentData.Notification;
            string intentData = notification.IntentData;

            Debug.Log($"Opened from notification {id} {channel} {intentData}");

            Dictionary<string, object> payload = new Dictionary<string, object> { { "intent", intentData } };
            AnalyticsProvider.Get.LogEvent("ClickedNotification", payload);
        }
#endif
    }

    private void ScheduleLocalNotifications()
    {
#if UNITY_ANDROID
        var notification = new AndroidNotification();

        notification.Title = $"It's time to solve some puzzles!";
        notification.Text = "A new set of daily challenges are available";
        notification.FireTime = DateTime.Now + TimeSpan.FromHours(24);
        notification.IntentData = "24hr reminder";

        Debug.Log($"Scheduled notification {notification.Title} for {notification.FireTime}");
        AndroidNotificationCenter.SendNotification(notification, "timers");
#endif
    }

    private void OnApplicationFocus(bool hasFocus)
    {
        if (!hasFocus)
        {
            return;
        }

#if UNITY_ANDROID
        AndroidNotificationCenter.CancelAllNotifications();
#endif
    }
}