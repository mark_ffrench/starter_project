﻿using System;
using System.Collections.Generic;
using Facebook.Unity;
using Facebook.Unity.Example;
using Firebase.Auth;
using Framework;

#if UNITY_ANDROID
using GooglePlayGames;
using GooglePlayGames.BasicApi;
#endif

using Helpers;
using TMPro;
using UI;
using UnityEngine;
using UnityEngine.UI;

public class BootScreen : UIScreen
{
    [SerializeField] private FillBar progressBar;
    [SerializeField] private Button GooglePlayLoginButton;
    [SerializeField] private Button AnonLoginButton;
    [SerializeField] private GameObject loginContainer;
    [SerializeField] private TextMeshProUGUI loginErrorMessage;

    private const int GPlayLogin = 1;
    private const int AnonLogin = 2;
    private const int FBLogin = 3;

    public event Action OnContinueBoot;

    private AuthProvider authProvider;
    private bool attemptGPlayAuth = false;
    private bool needToInvokeContinue;

    protected override void OnShow()
    {
        loginContainer.SetActive(false);
        loginErrorMessage.text = string.Empty;
    }

    private void OnInitComplete()
    {
        string logMessage = $"FB OnInitCompleteCalled IsLoggedIn='{FB.IsLoggedIn}' IsInitialized='{FB.IsInitialized}'";
        Debug.Log(logMessage);

        if (AccessToken.CurrentAccessToken != null)
        {
            Debug.Log($"FB Has Access Token: {AccessToken.CurrentAccessToken}");
        }
        
        FB.Mobile.FetchDeferredAppLinkData(DeepLinkCallback);
        
        int loginMethod = PlayerPrefs.GetInt("AuthMethod", 0);
        loginContainer.SetActive(loginMethod == 0);

        if (loginMethod == GPlayLogin)
        {
            LoginGoogle();
        }
        else if (loginMethod == AnonLogin)
        {
            LoginAnon();
        }
        else if (loginMethod == FBLogin)
        {
            LoginFacebook();
        }
    }

    public void SetProgress(int current, int max)
    {
        progressBar.SetWithInts(current, max);
    }
    
    private void OnHideUnity(bool isGameShown)
    {
        Debug.Log($"FB Is game shown: {isGameShown}");
    }

    public void AfterConsent()
    {
        if (authProvider == null)
        {
            authProvider = GameObjectFactory.Create<AuthProvider>();
        }

        FB.Init(OnInitComplete, OnHideUnity);
    }

    private void DeepLinkCallback(IAppLinkResult result)
    {
        if (result != null)
        {
            Debug.Log($"DeepLink Data\n" +
                      $"URL: {result.Url}\n" +
                      $"Target URL: {result.TargetUrl}\n" +
                      $"Ref: {result.Ref}");

            if (result.Extras != null)
            {
                foreach (KeyValuePair<string, object> kvp in result.Extras)
                {
                    Debug.Log($"DeepLink Extra: {kvp.Key}:{kvp.Value}");
                }
            }
        }
    }

    public void LoginFacebook()
    {
        GooglePlayLoginButton.interactable = false;
        AnonLoginButton.interactable = false;
        loginErrorMessage.text = string.Empty;

        List<string> scopes = new List<string>
        {
            "public_profile",
            "user_friends"
        };
        
#if UNITY_IOS
        //TODO: iOS Check ATT opt-in choice
        LoginTracking mode = LoginTracking.LIMITED;
        
        if (mode == LoginTracking.ENABLED)
        {
            FB.Mobile.LoginWithTrackingPreference(LoginTracking.ENABLED, scopes, "dtp_nonce456", OnFBLogin);    
        }
        else // mode == loginTracking.LIMITED
        {
            FB.Mobile.LoginWithTrackingPreference(LoginTracking.LIMITED, scopes, "dtp_nonce123", OnFBLogin);
        }
#else
        FB.LogInWithReadPermissions(scopes, OnFBLogin);
#endif
    }

    public void LoginGoogle()
    {
#if UNITY_ANDROID
        Debug.Log("initializing play games platform - hope it's not a dupe");
        PlayGamesClientConfiguration config = new PlayGamesClientConfiguration.Builder()
            .RequestServerAuthCode(false /* Don't force refresh */)
            .Build();

        PlayGamesPlatform.InitializeInstance(config);
        PlayGamesPlatform.Activate();

        GooglePlayLoginButton.interactable = false;
        AnonLoginButton.interactable = false;
        loginErrorMessage.text = string.Empty;

        Social.localUser.Authenticate((success, error) =>
        {
            // handle success or failure
            Debug.Log($"Google play login success: {success}");

            if (success)
            {
                attemptGPlayAuth = true;
            }
            else
            {
                loginContainer.SetActive(true);
                loginErrorMessage.text = error;
                GooglePlayLoginButton.interactable = true;
                AnonLoginButton.interactable = true;
                Debug.LogError(error);
            }
        });
#else
        Debug.LogError("Google login not supported on this platform");
#endif
    }

    private void Update()
    {
        if (attemptGPlayAuth)
        {
            Debug.Log("Attempting gplayauth");
            attemptGPlayAuth = false;
            authGPlay();
        }

        if (needToInvokeContinue)
        {
            string userID = AuthProvider.UserID();

            AnalyticsProvider.Get.Login(userID);

            needToInvokeContinue = false;
            loginContainer.SetActive(false);
            OnContinueBoot?.Invoke();
        }
    }

    private async void authGPlay()
    {
#if UNITY_ANDROID
        string authCode = PlayGamesPlatform.Instance.GetServerAuthCode();
        PlayerPrefs.SetInt("AuthMethod", GPlayLogin);
        authProvider.OnSignedIn += OnAuthComplete;
        await authProvider.GooglePlayLogin(authCode);
#else
        Debug.LogError("Google play auth not supported on this platform");
#endif
    }

    private void OnFBLogin(ILoginResult result)
    {
        if (result == null)
        {
            Debug.LogError("Got a null result from facebook login");
            ClearLoginMethod();
            return;
        }
        
        if (!string.IsNullOrEmpty(result.Error))
        {
            FB.LogOut();
            ClearLoginMethod();

            loginErrorMessage.text = result.Error;
            Debug.LogError($"Result.Error: \"{result.Error}\"");
        }

        Debug.Log("Player cancelled login: " + result.Cancelled);
        Debug.Log("Auth token: " + result.AuthenticationToken);
        
        if (result.AccessToken != null)
        {
            Debug.Log("Access token: " + result.AccessToken.TokenString);
            PlayerPrefs.SetInt("AuthMethod", FBLogin);
            authProvider.OnSignedIn += OnAuthComplete;
            authProvider.FacebookLogin(result.AccessToken);
        }
        else
        {
            ClearLoginMethod();
        }
    }

    private void ClearLoginMethod()
    {
        PlayerPrefs.SetInt("AuthMethod", 0);
        loginContainer.SetActive(true);
        GooglePlayLoginButton.interactable = true;
        AnonLoginButton.interactable = true;
    }

    public async void LoginAnon()
    {
        GooglePlayLoginButton.interactable = false;
        AnonLoginButton.interactable = false;

        PlayerPrefs.SetInt("AuthMethod", AnonLogin);
        authProvider.OnSignedIn += OnAuthComplete;
        await authProvider.AnonLogin();
    }

    private void OnAuthComplete()
    {
        needToInvokeContinue = true;
        authProvider.OnSignedIn -= OnAuthComplete;
    }
}