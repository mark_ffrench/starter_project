using System;
using System.Collections.Generic;
using System.Reflection;

public static class DependencyResolver
{
    private static Dictionary<Type, object> dependencies = new Dictionary<Type, object>();
        
    public static void AddDependency(object obj)
    {
        if (dependencies.ContainsKey(obj.GetType()))
        {
            throw new Exception($"Duplicate dependency declared for type {obj.GetType()}");
        }
            
        dependencies.Add(obj.GetType(), obj);
    }
        
    public static void AddDependency<T>(object obj) 
    {
        if (dependencies.ContainsKey(typeof(T)))
        {
            throw new Exception($"Duplicate dependency declared for type {typeof(T)}");
        }
            
        dependencies.Add(typeof(T), obj);
    }
        
    public static void InjectDependencies(Object obj)
    {
        Type monoType = obj.GetType();

        List<FieldInfo> objectFields = new List<FieldInfo>();
            
        objectFields.AddRange(monoType.GetFields(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic));

        while (monoType.BaseType != null)
        {
            objectFields.AddRange(monoType.BaseType.GetFields(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic));
            monoType = monoType.BaseType;
        }
            
        foreach (FieldInfo fieldInfo in objectFields)
        {
            if (Attribute.GetCustomAttribute(fieldInfo, typeof(InjectAttribute)) is InjectAttribute)
            {
                if (dependencies.TryGetValue(fieldInfo.FieldType, out object asset))
                {
                    fieldInfo.SetValue(obj, asset);
                }
                else
                {
                    throw new Exception($"No available instance of {fieldInfo.FieldType} to inject into {obj}");
                }
            }
        }
    }
}