using System;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Helpers
{
    public static class GameObjectFactory
    {
        public static T Create<T>(string name = null) where T : MonoBehaviour
        {
            if (name == null)
                name = typeof(T).Name;
            
            GameObject go = new GameObject(name);
            T monoBehaviour = go.AddComponent<T>();
            DependencyResolver.InjectDependencies(monoBehaviour);
            return monoBehaviour;
        }
        
        public static T Create<T>(T prefab, Transform parent = null) where T : MonoBehaviour
        {
            if(prefab == null)
                throw new ArgumentNullException(nameof(prefab), "Passed null prefab in to GameObjectFactor.Create()");
            
            T monoBehaviour = Object.Instantiate(prefab, parent);
            DependencyResolver.InjectDependencies(monoBehaviour);
            return monoBehaviour;
        }
    }
}