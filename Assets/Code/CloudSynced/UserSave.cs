using System;
using Framework;
using Helpers;
using Newtonsoft.Json;
using UnityEngine;
using Random = UnityEngine.Random;

public class UserSave : CloudSyncedObject
{
    public int numGems;
    public int numCoins;
    public int energy;
    public long energyTouchedAt;

    public int xp;
    public int currentLevel;
    
    public string name;
    [JsonIgnore] public string socialID;

    public bool AdsRemoved;
    public int AdsAvoided;

    public int NumDaysPlayed;
    public int NumSessions;
    public int NumDailyPuzzles;
    
    public int InterstitialsWatched;
    public int RewardedAdsWatched;
    public int NumPurchases;
    public Decimal EstimatedSpend;
    
    public const string CollectionName = "users";

    public FTUEFlags FTUEFLags = FTUEFlags.None;

    [Flags]
    public enum FTUEFlags
    {
        None = 0,
        FTUEComplete = 1,
        BetaNoticeServed = 2,
        RateGamePopupShown = 4,
    }

    public override string GetCollectionName()
    {
        return CollectionName;
    }

    public override void SetDefaults()
    {
        numGems = 100;
        numCoins = 2000;

        AdsRemoved = false;
        FTUEFLags = FTUEFlags.None;
        
        currentLevel = 1;

        name = GenerateGuestName();
    }

    public void SetCurrency(string type, int amount, string source = "Unknown")
    {
        switch (type)
        {
            case CurrencyAccount.Gems:
                numGems = amount;
                break;
            case CurrencyAccount.Coins:
                numCoins = amount;
                break;
            default:
                throw new ArgumentOutOfRangeException($"Unknown currency type {type}");
        }

        IsDirty = true;
        AnalyticsProvider.Get.OnUserUpdated(this);
    }

    public int GetCurrency(string type)
    {
        switch (type)
        {
            case CurrencyAccount.Gems:
                return numGems;
            case CurrencyAccount.Coins:
                return numCoins;
        }

        throw new ArgumentOutOfRangeException($"Unknown currency type {type}");
    }

    public void SetFTUEFlag(FTUEFlags flag)
    {
        FTUEFLags |= flag;
        IsDirty = true;
    }

    public bool IsFlagSet(FTUEFlags flag)
    {
        return (FTUEFLags & flag) != 0;
    }

    private static string GenerateGuestName()
    {
        return "Guest" + Random.Range(10000, 99999);
    }

    public void WatchedInterstitialAd()
    {
        InterstitialsWatched++;
        IsDirty = true;
    }

    public void LogMissedInterstitialAd()
    {
        AdsAvoided++;
        IsDirty = true;
    }
    
    public void WatchedRewardedAd()
    {
        RewardedAdsWatched++;
        IsDirty = true;
    }
    
    public void LogPurchase(Decimal value)
    {
        NumPurchases++;
        EstimatedSpend += value;
        IsDirty = true;
    }

    public void RecordNewSession(bool isNewDay)
    {
        NumSessions++;

        if (isNewDay)
        {
            NumDaysPlayed++;
        }

        IsDirty = true;
    }
}