using System;

namespace Helpers
{
    public class LoginResult
    {
        public bool Success = false;
        public TimeSpan TimeSinceLastSession = TimeSpan.Zero;
        public bool IsNewDay = true;
    }
}