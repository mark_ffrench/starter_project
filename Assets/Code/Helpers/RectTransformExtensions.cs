using UnityEngine;

namespace Helpers
{
    public static class RectTransformExtensions
    {
        public static Rect ToScreenSpaceRect(this RectTransform transform)
        {
            Vector2 size = Vector2.Scale(transform.rect.size, transform.lossyScale);
            Rect rect = new Rect(transform.position.x, Screen.height - transform.position.y, size.x, size.y);
            rect.x -= (transform.pivot.x * size.x);
            rect.y -= ((1.0f - transform.pivot.y) * size.y);
            return rect;
        }
        
        public static Rect RectTransformToScreenSpace(this RectTransform transform)
        {
            Vector2 size = transform.rect.size; // Vector2.Scale(transform.rect.size, transform.lossyScale);
 
            float newRectX = transform.position.x + (Screen.width / 2) - (size.x * (1 - transform.pivot.x));
            float newRectY = Mathf.Abs(transform.position.y - (Screen.height / 2)) - (size.y * (1 - transform.pivot.y));
 
            return new Rect(newRectX, newRectY, size.x, size.y);
        }

        public static Vector2 GetScreenPos(this RectTransform transform)
        {
            return RectTransformUtility.WorldToScreenPoint(null, transform.transform.position);
        }
        
//        public static void SetPivot(this RectTransform rectTransform, Vector2 pivot)
//        {
//            if (rectTransform == null) return;
// 
//            Vector2 size = rectTransform.rect.size;
//            Vector2 deltaPivot = rectTransform.pivot - pivot;
//            Vector3 deltaPosition = new Vector3(deltaPivot.x * size.x, deltaPivot.y * size.y);
//            rectTransform.pivot = pivot;
//            rectTransform.localPosition -= deltaPosition;
//        }
        
        public static void SetPivot(this RectTransform target, Vector2 pivot)
        {
            if (!target) return;
            var offset=pivot - target.pivot;
            offset.Scale(target.rect.size);
            var wordlPos= target.position + target.TransformVector(offset);
            target.pivot = pivot;
            target.position = wordlPos;
        }
    }
}