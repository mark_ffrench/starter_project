/*
Simplified BSD License
 
Copyright 2017 Derek Will
 
Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 
1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 
2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer 
in the documentation and/or other materials provided with the distribution.
 
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, 
OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, 
OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
OF SUCH DAMAGE.
*/

using System;

namespace Helpers
{
    //original - from https://derekwill.com/2015/03/05/bit-processing-in-c/
    public static partial class ByteExtensions
    {
        public static bool IsBitSet(this byte b, int pos)
        {
            if (pos < 0 || pos > 7)
                throw new ArgumentOutOfRangeException(nameof(pos), "Index must be in the range of 0-7.");

            return (b & (1 << pos)) != 0;
        }

        public static byte WithBit(this byte b, int pos)
        {
            if (pos < 0 || pos > 7)
                throw new ArgumentOutOfRangeException(nameof(pos), "Index must be in the range of 0-7.");

            return (byte) (b | (1 << pos));
        }

        public static byte WithoutBit(this byte b, int pos)
        {
            if (pos < 0 || pos > 7)
                throw new ArgumentOutOfRangeException(nameof(pos), "Index must be in the range of 0-7.");

            return (byte) (b & ~(1 << pos));
        }

        public static byte WithToggledBit(this byte b, int pos)
        {
            if (pos < 0 || pos > 7)
                throw new ArgumentOutOfRangeException(nameof(pos), "Index must be in the range of 0-7.");

            return (byte) (b ^ (1 << pos));
        }

        public static string ToBinaryString(this byte b)
        {
            return Convert.ToString(b, 2).PadLeft(8, '0');
        }

        public static byte WithBit(this byte b, int pos, bool set)
        {
            return set ? b.WithBit(pos) : b.WithoutBit(pos);
        }
        
        public static byte ReadValue(this byte b, int startIndex, int bitLength)
        {
            if (startIndex < 0 || startIndex > 7)
                throw new ArgumentOutOfRangeException(nameof(startIndex), "Index must be in the range of 0-7.");
            
            if (startIndex+bitLength < 1 || startIndex+bitLength > 8)
                throw new ArgumentOutOfRangeException(nameof(bitLength), "index+length must be in the range of 1-8.");
            
            //shift bits along (truncating anything to the right of the n bits we're interested in)
            int shifted = b >> startIndex;
            
            //create a mask n bits long e.g. 00000111;
            int mask = (1 << bitLength) - 1;
            
            //perform AND operation & 31 (11111) which clears all bits to the left of the n bits we're interested in
            return (byte)(shifted & mask);
        }
        
        public static byte WithValue(this byte b, int startIndex, int bitLength, int value)
        {
            if (startIndex < 0 || startIndex > 7)
                throw new ArgumentOutOfRangeException(nameof(startIndex), "Index must be in the range of 0-7.");
            
            if (startIndex+bitLength < 1 || startIndex+bitLength > 8)
                throw new ArgumentOutOfRangeException(nameof(bitLength), "index+length must be in the range of 1-7.");
            
            if(value < 0 || value >= 1 << bitLength)
                throw new ArgumentOutOfRangeException(nameof(value), $"Can't fit {value} into {bitLength} bits");
            
            //create a mask n bits long e.g. 00000111;
            int mask = (1 << bitLength) - 1;
                                                  
            //5 bits have been shifted left to the correct position (everything else is 0s)
            int valueShiftedToTheLeft = value << startIndex;

            //Mask for just the slot we're interested in
            //00000111110000000000
            mask = mask << startIndex;
                    
            //invert the mask
            //11111000001111111111
            mask = ~mask;
                    
            //AND comes before OR
            //The & mask clears anything in the n bits we're interested in
            //The | value combines our new CarIndex
            return (byte)(b & mask | valueShiftedToTheLeft);
        }
    }
}