using System;

namespace Helpers
{
    public static class TimespanFormatter
    {
        public static string ToTimerString(this TimeSpan timeSpan)
        {
            if (timeSpan.TotalDays > 2)
            {
                return $"{timeSpan.TotalDays:N0}d {timeSpan.Hours:00}h {timeSpan.Minutes:00}m";
            }

            if (timeSpan.TotalHours > 1)
            {
                return $"{timeSpan.Hours:N0}h {timeSpan.Minutes:00}m {timeSpan.Seconds:00}s";
            }
            
            if (timeSpan.TotalMinutes > 1)
            {
                return $"{timeSpan.Minutes:N0}m {timeSpan.Seconds:00}s";
            }
            
            return $"{timeSpan.TotalSeconds:N0}s";
        }
        
        public static string ToDigitalTimerString(this TimeSpan timeSpan)
        {
            if (timeSpan.TotalDays > 2)
            {
                return $"{timeSpan.TotalDays:N0}:{timeSpan.Hours:00}:{timeSpan.Minutes:00}:{timeSpan.Seconds:00}";
            }

            if (timeSpan.TotalHours > 1)
            {
                return $"{timeSpan.Hours:N0}:{timeSpan.Minutes:00}:{timeSpan.Seconds:00}";
            }
            
            if (timeSpan.TotalMinutes > 1)
            {
                return $"{timeSpan.Minutes:N0}:{timeSpan.Seconds:00}";
            }
           
            return $"0:{timeSpan.TotalSeconds:00}";
        }
    }
}