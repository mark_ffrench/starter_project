using System;

public static class LongExtensions
{
    public const int k_longLength = 64;
    private const int k_maxIndex = k_longLength - 1;

    private static void checkIndex(int pos)
    {
        if (pos < 0 || pos > k_maxIndex)
            throw new ArgumentOutOfRangeException(nameof(pos), $"Index must be in the range of 0-{k_maxIndex}.");
    }

    private static void checkIndexRange(int startIndex, int bitLength)
    {
        checkIndex(startIndex);

        if (startIndex + bitLength < 1 || startIndex + bitLength > k_longLength)
            throw new ArgumentOutOfRangeException(nameof(bitLength),
                $"index+length must be in the range of 1-{k_longLength}.");
    }

    public static bool IsBitSet(this long b, int pos)
    {
        checkIndex(pos);
        return (b & (1U << pos)) != 0;
    }

    public static long WithBit(this long b, int pos)
    {
        checkIndex(pos);
        return (long) (b | (1U << pos));
    }

    public static long WithoutBit(this long b, int pos)
    {
        checkIndex(pos);
        return (long) (b & ~(1U << pos));
    }

    public static long WithToggledBit(this long b, int pos)
    {
        checkIndex(pos);
        return (long) (b ^ (1U << pos));
    }

    public static string ToBinaryString(this long b)
    {
        return Convert.ToString(b, 2).PadLeft(k_maxIndex, '0');
    }

    public static long WithBit(this long b, int pos, bool set)
    {
        return set ? b.WithBit(pos) : b.WithoutBit(pos);
    }

    public static long ReadValue(this long b, int startIndex, int bitLength)
    {
        checkIndexRange(startIndex, bitLength);

        //shift bits along (truncating anything to the right of the n bits we're interested in)
        long shifted = b >> startIndex;

        //create a mask n bits long e.g. 00000111;
        long mask = (1 << bitLength) - 1;

        //perform AND operation & 31 (11111) which clears all bits to the left of the n bits we're interested in
        return (long) (shifted & mask);
    }

    public static long WithValue(this long b, int startIndex, int bitLength, int value)
    {
        checkIndexRange(startIndex, bitLength);

        if (value < 0 || value >= 1 << bitLength)
            throw new ArgumentOutOfRangeException(nameof(value), $"Can't fit {value} into {bitLength} bits");

        //create a mask n bits long e.g. 00000111;
        long mask = (1 << bitLength) - 1;

        //5 bits have been shifted left to the correct position (everything else is 0s)
        long valueShiftedToTheLeft = value << startIndex;

        //Mask for just the slot we're interested in
        //00000111110000000000
        mask = mask << startIndex;

        //invert the mask
        //11111000001111111111
        mask = ~mask;

        //AND comes before OR
        //The & mask clears anything in the n bits we're interested in
        //The | value combines our new CarIndex
        return (long) (b & mask | valueShiftedToTheLeft);
    }
}