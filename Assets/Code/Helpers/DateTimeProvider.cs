using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Firebase.Auth;
using Firebase.Database;
using UnityEngine;

namespace Helpers
{
    //Fetches the datetime according to Firebase, to avoid using the local clock.
    public class DateTimeProvider
    {
        private static float loginSessionTime = -1.0f;
        private static DateTime loginServerDateTime;

        public static async Task<LoginResult> WriteLoginTimestamp()
        {
            string userId = FirebaseAuth.DefaultInstance.CurrentUser.UserId;
        
            DatabaseReference reference = FirebaseDatabase.DefaultInstance.RootReference;

            List<Task> tasks = new List<Task>();

            DatabaseReference loginTimestampRef = reference.Child("login").Child(userId);
            DataSnapshot before = await loginTimestampRef.GetValueAsync();
            
            DateTimeOffset lastLogin = DateTimeOffset.MinValue;
            
            string oldTimestampString = before.GetRawJsonValue();
            if (long.TryParse(oldTimestampString, out long oldTimestamp))
            {
                lastLogin = DateTimeOffset.FromUnixTimeMilliseconds(oldTimestamp);
            }
            
            Task task = loginTimestampRef.SetValueAsync(ServerValue.Timestamp);
            tasks.Add(task);
            
            int timeout = 10000;
            Task timeoutTask = Task.Delay(timeout);

            if (await Task.WhenAny(task, timeoutTask) == timeoutTask)
            {
                Debug.LogError("Timed out");
                return new LoginResult {Success = false};
            }
            else
            {
                Debug.Log("All good in the hood");
            }
            
            DataSnapshot after = await loginTimestampRef.GetValueAsync();

            string timestampString = after.GetRawJsonValue();
            if (long.TryParse(timestampString, out long timestamp))
            {
                DateTimeOffset offset = DateTimeOffset.FromUnixTimeMilliseconds(timestamp);
                loginServerDateTime = offset.UtcDateTime;
            
                loginSessionTime = Time.realtimeSinceStartup;
            }

            if (lastLogin != DateTimeOffset.MinValue)
            {
                return new LoginResult
                {
                    Success = true, 
                    TimeSinceLastSession = loginServerDateTime - lastLogin,
                    IsNewDay = loginServerDateTime.Date != lastLogin.Date
                };
            }
            else
            {
                return new LoginResult
                {
                    Success = true
                };
            }
        }
        
        public static DateTime Now
        {
            get
            {
                if (loginSessionTime > -1)
                {
                    return loginServerDateTime + TimeSpan.FromSeconds(Time.realtimeSinceStartup - loginSessionTime);
                }
                
                Debug.LogError("Using fallback time");
                return DateTime.UtcNow;
            }
        }

        public static DateTime EndOfServerDay
        {
            get
            {
                DateTime serverTime = Now;
                return new DateTime(serverTime.Year, serverTime.Month, serverTime.Day + 1, 0, 0, 0);
            }
        }

        public static DateTime StartOfServerDay
        {
            get
            {
                DateTime serverTime = Now;
                return new DateTime(serverTime.Year, serverTime.Month, serverTime.Day, 0, 0, 0);
            }
        }
    }
}