﻿using System;
using System.Linq;

namespace Helpers
{
    public static class EnumExt
    {
        public static T[] EnumValues<T>() where T : struct
        {
            var type = typeof(T);
            if(!type.IsEnum) throw new ArgumentException("Only pass in enums to this method");

            return Enum.GetValues(type).Cast<T>().ToArray();
        }

        public static T Parse<T>(string val) where T : struct
        {
            var type = typeof(T);
            if(!type.IsEnum) throw new ArgumentException("Only pass in enums to this method");

            if (Enum.TryParse(val, out T enumVal))
            {
                return enumVal;
            }
            else
            {
                throw new Exception($"Could not parse {val} as an enum of type {type}");
            }
        }
        
        public static int NumberOfValues<T>() where  T : struct
        {
            return Enum.GetValues(typeof(T)).Length;
        }
        
        public static T NextValue<T>(this T src) where T : struct
        {
            T[] values = EnumValues<T>().ToArray();
            
            int next = Array.IndexOf(values, src) + 1;

            if (next >= values.Length)
            {
                return values[0];
            }

            return values[next];
        }
        
        public static T PrevValue<T>(this T src) where T : struct
        {
            T[] values = EnumValues<T>().ToArray();
            
            int prev = Array.IndexOf(values, src) - 1;

            if (prev < 0)
            {
                return values[values.Length-1];
            }

            return values[prev];
        }
    }
}