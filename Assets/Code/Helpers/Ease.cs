﻿using System;
using System.Collections;
using UnityEngine;

namespace Helpers
{
    public class Ease : MonoBehaviour
    {
        private static Ease m_instance;
        
        private static Ease Instance
        {
            get
            {
                if (m_instance == null)
                {
                    GameObject go = new GameObject("Easing");
                    m_instance = go.AddComponent<Ease>();
                }

                return m_instance;
            }
        }
        
        public static Coroutine EaseTarget(float animDuration, Func<float, float> ease, float delay,
            Action<float> Update, Action OnFinished = null)
        {
            Coroutine coroutine = Instance.StartCoroutine(DoEase(delay, animDuration, Update, ease, OnFinished));
            return coroutine;
        }
        
        private static IEnumerator DoEase(float delay, float animDuration, Action<float> update, Func<float,float> ease, Action OnFinished)
        {
            float animTimer = 0f;
            update(animTimer);
        
            yield return new WaitForSeconds(delay);
        
            while (animTimer < animDuration)
            {
                float t = animTimer / animDuration;
                t = ease(t);
                
                update(t);
            
                yield return new WaitForEndOfFrame();
                animTimer += Time.deltaTime;
            }
            
            OnFinished?.Invoke();
        }
        
        public static Coroutine PulseTarget(float animDuration, Func<float, float> easeIn, Func<float, float> easeOut, float delay,
            Action<float> Update, Action OnFinished = null)
        {
            Coroutine coroutine = Instance.StartCoroutine(DoPulse(delay, animDuration, Update, easeIn, easeOut, OnFinished));
            return coroutine;
        }
        
        private static IEnumerator DoPulse(float delay, float animDuration, Action<float> update, Func<float,float> easeIn, Func<float,float> easeOut, Action OnFinished)
        {
            float animTimer = 0f;
            update(animTimer);
        
            yield return new WaitForSeconds(delay);
        
            while (animTimer < animDuration)
            {
                float t = animTimer / animDuration;

                if (t < 0.5f)
                {
                    //count up from 0 to 1
                    t = easeIn(t * 2);
                }
                else
                {
                    //count down from 1 to 0
                    t = (t - 0.5f) * 2;
                    t = easeOut(1f - t);
                }
                
                update(t);
            
                yield return new WaitForEndOfFrame();
                animTimer += Time.deltaTime;
            }
            
            OnFinished?.Invoke();
        }
    }
}