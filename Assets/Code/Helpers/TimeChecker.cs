using System;
using System.IO;
using System.Net;
using System.Threading.Tasks;
using Newtonsoft.Json;
using UnityEngine;

namespace Helpers
{
    public class TimeChecker
    {
        private struct DateTimeResponse
        {
            public string currentDateTime;
        }

        private static DateTime lastServerTime;
        private static DateTime timeChecked;

        public static DateTime CurrentServerTime()
        {
            TimeSpan elapsed = DateTime.UtcNow - timeChecked;
            return lastServerTime + elapsed;
        }

        public async Task GetDateTime()
        {
            Debug.Log("Checking time...");
            string url = "http://worldclockapi.com/api/json/utc/now";
            try
            {
                string jsonResponse = await GetAsync(url);
                
                Debug.Log("read finished");
                Debug.Log(jsonResponse);
                DateTimeResponse response = JsonConvert.DeserializeObject<DateTimeResponse>(jsonResponse);

                if (DateTime.TryParse(response.currentDateTime, out DateTime dateTime))
                {
                    Debug.Log("Server time is " + dateTime);
                    lastServerTime = dateTime;
                    timeChecked = DateTime.UtcNow;
                }
                else
                {
                    Debug.LogError("Couldn't parse server datetime'");
                }
            }
            catch (WebException ex)
            {
                Debug.Log("Couldn't reach server time endpoint: "+ex.Message);
                lastServerTime = DateTime.UtcNow;
                timeChecked = DateTime.UtcNow;
            }


        }

        public async Task<string> GetAsync(string uri)
        {
            HttpWebRequest request = (HttpWebRequest) WebRequest.Create(uri);
            request.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;
            
            Debug.Log("created request, awaiting response");

            using (HttpWebResponse response = (HttpWebResponse) await request.GetResponseAsync())
            {
                Debug.Log("got response");
                
                using (Stream stream = response.GetResponseStream())
                {
                    using (StreamReader reader = new StreamReader(stream))
                    {
                        Debug.Log("reading response");
                        return await reader.ReadToEndAsync();
                    }
                }
            }
        }
    }
}