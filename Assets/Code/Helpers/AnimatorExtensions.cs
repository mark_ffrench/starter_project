using UnityEngine;

namespace Helpers
{
    public static class AnimatorExtensions
    {
        public static bool IsPlaying(this Animator animator)
        {
            for (int i = 0; i < animator.layerCount; i++)
            {
                AnimatorStateInfo state = animator.GetCurrentAnimatorStateInfo(i);
                if (state.loop || animator.IsInTransition(i) || state.normalizedTime < 1.0f)
                    return true;
            }

            return false;
        }
    }
}