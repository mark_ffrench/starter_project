using UnityEngine;
using System.Collections;

public class Fps : MonoBehaviour
{
    string label = "";
    float count;

    private GUIStyle style;

    IEnumerator Start()
    {
        style = new GUIStyle
        {
            font = Font.CreateDynamicFontFromOSFont("Arial", 30), 
            normal = {textColor = Color.white}
        };

        GUI.depth = 2;
        while (true)
        {
            if (Time.timeScale == 1)
            {
                yield return new WaitForSeconds(0.1f);
                count = (1 / Time.deltaTime);
                label = "FPS :" + (Mathf.Round(count));
            }
            else
            {
                label = "Pause";
            }

            yield return new WaitForSeconds(0.5f);
        }
    }

    void OnGUI()
    {
        GUI.Label(new Rect(10, Screen.height - 40, 100, 25), label, style);
    }
}