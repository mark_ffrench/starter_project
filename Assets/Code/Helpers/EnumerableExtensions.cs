﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using UI;
using UnityEngine;
using Object = UnityEngine.Object;
using Random = System.Random;

namespace Helpers
{
	
    /// <summary>
    /// Provides a set of handy game-centric extensions for enumerable types
    /// </summary>
	public static class EnumerableExtension
	{
	    public static T PickRandom<T>(this IEnumerable<T> source)
	    {
	        return source.PickRandom(1).Single();
	    }
	
	    public static IEnumerable<T> PickRandom<T>(this IEnumerable<T> source, int count)
	    {
	        return source.Shuffle().Take(count);
	    }
	    
	    public static T PopRandom<T>(this List<T> source)
	    {
		    return source.Shuffle().PopAt(source.Count() - 1);
	    }
	    
	    public static T PopAt<T>(this List<T> list, int index)
	    {
		    T r = list[index];
		    list.RemoveAt(index);
		    return r;
	    }
	
	    public static IEnumerable<T> Shuffle<T>(this IEnumerable<T> source)
	    {
	        return source.OrderBy(x => Guid.NewGuid());
	    }
	
		public static Stack<T> Shuffle<T>(this Stack<T> stack)
		{
			return new Stack<T>(stack.OrderBy(x => Guid.NewGuid()));
		}
		
		public static void PushRange<T>(this Stack<T> source, IEnumerable<T> collection)
		{
			foreach (T item in collection)
				source.Push(item);
		}
		
		public static Queue<T> Shuffle<T>(this Queue<T> queue)
		{
			return new Queue<T>(queue.OrderBy(x => Guid.NewGuid()));
		}
		
		public static void PushRange<T>(this Queue<T> source, IEnumerable<T> collection)
		{
			foreach (T item in collection)
				source.Enqueue(item);
		}
		
//		public static List<T> Shuffle<T>(this List<T> list)
//		{
//			return new List<T>(list.OrderBy(x => Guid.NewGuid()));
//		}
		
		public static List<T> Shuffle<T>(this List<T> ts) {
			Random rnd = new Random();
			var count = ts.Count;
			var last = count - 1;
			for (var i = 0; i < last; ++i) {
				var r = rnd.Next(i, count);
				var tmp = ts[i];
				ts[i] = ts[r];
				ts[r] = tmp;
			}

			return ts;
		}
	
	    public static TValue GetValueOrDefault<TKey, TValue>
	    (this IDictionary<TKey, TValue> dictionary,
	     TKey key,
	     TValue defaultValue)
	    {
	        TValue value;
	        return dictionary.TryGetValue(key, out value) ? value : defaultValue;
	    }

	    public static IEnumerable<TResult> LeftOuterJoin<TSource, TInner, TKey, TResult>(this IEnumerable<TSource> source, IEnumerable<TInner> other, Func<TSource, TKey> func, Func<TInner, TKey> innerkey, Func<TSource, TInner, TResult> res)
	    {
	        return from f in source
	            join b in other on func.Invoke(f) equals innerkey.Invoke(b) into g
	            from result in g.DefaultIfEmpty()
	            select res.Invoke(f, result);
	    }
		
		public static T[] Populate<T>(this T[] arr, T value ) where T : struct
		{
			for ( int i = 0; i < arr.Length;i++ ) {
				arr[i] = value;
			}
			return arr;
		}
		
		public static T[] Populate<T>(this T[] arr, Func<T> func ) 
		{
			for ( int i = 0; i < arr.Length;i++ ) {
				arr[i] = func();
			}
			return arr;
		}
		
		public static bool ContainsDuplicates<T>(this IEnumerable<T> clues)
		{
			return clues.GroupBy(x => x.GetHashCode()).Any(g => g.Count() > 1);
		}
		
		public static IEnumerable<T> WithoutDuplicates<T>(this IEnumerable<T> clues)
		{
			return clues.GroupBy(x => x.GetHashCode()).Select(x => x.First());
		}
		
		public static void DestroyAll<T>(this List<T> scripts) where T : MonoBehaviour
		{
			foreach (T script in scripts)
			{
				Object.Destroy(script.gameObject);
			}
			
			scripts.Clear();
		}
		
		public static void DestroyAll(this List<GameObject> objects)
		{
			foreach (GameObject obj in objects)
			{
				Object.Destroy(obj);
			}
			
			objects.Clear();
		}

		public static bool IsValidIndex<T>(this T[] arr, int index)
		{
			if(arr == null)
				throw new ArgumentNullException();
			
			if (index < 0)
				return false;

			if (index >= arr.Length)
				return false;

			return true;
		}
		
		public static bool IsValidIndex<T>(this List<T> list, int index)
		{
			if(list == null)
				throw new ArgumentNullException();
			
			if (index < 0)
				return false;

			if (index >= list.Count)
				return false;

			return true;
		}

		public static bool TryGetAndCastValue<T, U>(this Dictionary<U, object> record, U key, out T value)
		{
			if (record.TryGetValue(key, out object val))
			{
				if (val is T castVal)
				{
					value = castVal;
					return true;
				}
				
				Debug.LogError($"key is an object of type {val.GetType()}, NOT a {typeof(T)}");
			}

			value = default(T);
			return false;
		}
		
		public static bool TryGetAndCastValue<U>(this Dictionary<U, object> record, U key, out float value)
		{
			if (record.TryGetValue(key, out object val))
			{
				switch (val)
				{
					case float floatVal:
						value = floatVal;
						return true;
					case double doubleVal:
						value = (float)doubleVal;
						return true;
					case long longVal:
						value = longVal;
						return true;
					case int intVal:
						value = intVal;
						return true;
					default:
						Debug.LogError($"key is an object of type {val.GetType()}, NOT a float");
						break;
				}
			}
			
			value = default;
			return false;
		}
		

	}
}
