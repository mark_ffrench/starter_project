using System;

public static class UShortExtensions
{
    private const int k_shortLength = 16;
    private const int k_maxIndex = k_shortLength - 1;

    private static void checkIndex(int pos)
    {
        if (pos < 0 || pos > k_maxIndex)
            throw new ArgumentOutOfRangeException(nameof(pos), $"Index must be in the range of 0-{k_maxIndex}.");
    }

    private static void checkIndexRange(int startIndex, int bitLength)
    {
        checkIndex(startIndex);

        if (startIndex + bitLength < 1 || startIndex + bitLength > k_shortLength)
            throw new ArgumentOutOfRangeException(nameof(bitLength),
                $"index+length must be in the range of 1-{k_shortLength}.");
    }

    public static bool IsBitSet(this ushort b, int pos)
    {
        checkIndex(pos);
        return (b & (1 << pos)) != 0;
    }

    public static ushort WithBit(this ushort b, int pos)
    {
        checkIndex(pos);
        return (ushort) (b | (1 << pos));
    }

    public static ushort WithoutBit(this ushort b, int pos)
    {
        checkIndex(pos);
        return (ushort) (b & ~(1 << pos));
    }

    public static ushort WithToggledBit(this ushort b, int pos)
    {
        checkIndex(pos);
        return (ushort) (b ^ (1 << pos));
    }

    public static string ToBinaryString(this ushort b)
    {
        return Convert.ToString(b, 2).PadLeft(k_maxIndex, '0');
    }

    public static ushort WithBit(this ushort b, int pos, bool set)
    {
        return set ? b.WithBit(pos) : b.WithoutBit(pos);
    }

    public static ushort ReadValue(this ushort b, int startIndex, int bitLength)
    {
        checkIndexRange(startIndex, bitLength);

        //shift bits along (truncating anything to the right of the n bits we're interested in)
        int shifted = b >> startIndex;

        //create a mask n bits long e.g. 00000111;
        int mask = (1 << bitLength) - 1;

        //perform AND operation & 31 (11111) which clears all bits to the left of the n bits we're interested in
        return (ushort) (shifted & mask);
    }

    public static ushort WithValue(this ushort b, int startIndex, int bitLength, int value)
    {
        checkIndexRange(startIndex, bitLength);

        if (value < 0 || value >= 1 << bitLength)
            throw new ArgumentOutOfRangeException(nameof(value), $"Can't fit {value} into {bitLength} bits");

        //create a mask n bits long e.g. 00000111;
        int mask = (1 << bitLength) - 1;

        //5 bits have been shifted left to the correct position (everything else is 0s)
        int valueShiftedToTheLeft = value << startIndex;

        //Mask for just the slot we're interested in
        //00000111110000000000
        mask = mask << startIndex;

        //invert the mask
        //11111000001111111111
        mask = ~mask;

        //AND comes before OR
        //The & mask clears anything in the n bits we're interested in
        //The | value combines our new CarIndex
        return (ushort) (b & mask | valueShiftedToTheLeft);
    }
}