using System;

namespace Helpers
{
    public static class DateTimeExtensions
    {
        public static string CountdownAsString(this DateTime endDate)
        {
            TimeSpan remaining = endDate - DateTimeProvider.Now;
            return remaining.ToTimerString();
        }

        public static bool InThePast(this DateTime date)
        {
            return date <= DateTimeProvider.Now;
        }
        
        public static bool InTheFuture(this DateTime date)
        {
            return date > DateTimeProvider.Now;
        }

        public static bool IsAPreviousServerDay(this DateTime date)
        {
            return date < DateTimeProvider.StartOfServerDay;
        }
    }
}