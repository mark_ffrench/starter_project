﻿using System;
using System.Collections.Generic;
using System.Linq;
using UI;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Helpers
{
    public static class Probability
    {
        private static readonly System.Random Rnd = new System.Random();
        
        public static bool FlipCoin()
        {
            return Rnd.NextDouble() > 0.5;
        }

        public static double ChanceOverMultipleEvents(double overallProbability, int numEvents)
        {
            return Math.Pow(overallProbability, 1.0 / numEvents);
        }
    }
    
    public class Chance<TObject>
    {
        public TObject obj;
        public float probability;
        
        public static Chance<TObject> Of(TObject obj, float probability)
        {
            return new Chance<TObject>
            {
                obj = obj,
                probability = probability
            };
        }
	
        public static TObject PickRandom(params Chance<TObject>[] probabilities)
        {
            return PickRandomFromArray(probabilities);
        }
	
        public static TObject PickRandomFromArray(IEnumerable<Chance<TObject>> probabilities)
        {
            float total = 0;
            foreach (Chance<TObject> prob in probabilities)
            {
                prob.probability = Mathf.Max(0, prob.probability);
                total += prob.probability;
            }
	
            if (total <= 0)
            {
                return default(TObject);
            }
	
            float pick = Random.Range(0, total);
	
            int selection = 0;
	
            foreach (Chance<TObject> prob in probabilities)
            {
                pick -= prob.probability;
	
                if (pick <= 0)
                    return prob.obj;
            }
	
            return probabilities.First().obj;
        }
    }

    public class DropTable<TObject>
    {
        private List<Chance<TObject>> chances = new List<Chance<TObject>>();
            
        public void WithChanceOf(TObject result, float probability)
        {
            chances.Add(Chance<TObject>.Of(result, probability));
            
            float totalProb = chances.Sum(c => c.probability);

            if (totalProb > 1f)
            {
                throw new Exception("Elements of drop table should not add up to more than 1.00");
            }
        }

        public void OrElse(TObject result)
        {
            float totalProb = chances.Sum(c => c.probability);
            
            chances.Add(Chance<TObject>.Of(result, 1f-totalProb));
        }

        public TObject PickRandom()
        {
            return Chance<TObject>.PickRandomFromArray(chances);
        }
    }
}