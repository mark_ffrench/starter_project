﻿namespace Helpers
{
    public static class BoolExtensions
    {
        public static string ToOnOffString(this bool b)
        {
            return b ? "On" : "Off";
        }
    }
}