using System;
using System.Collections.Generic;
using System.Linq;

namespace Helpers
{
    public static class StringExtensions
    {
        public static IEnumerable<string> GetLines(this string str, bool removeEmptyLines = false)
        {
            return str.Split(new[] {"\r\n", "\r", "\n"},
                removeEmptyLines ? StringSplitOptions.RemoveEmptyEntries : StringSplitOptions.None);
        }

        public static IEnumerable<string[]> ParseCSV(this string str, bool removeEmptyLines = false)
        {
            return GetLines(str, removeEmptyLines).Select(l => l.Split(','));
        }
        
        public static string OrdinalSuffix(this int rank)
        {
            if (rank > 10 && rank < 20)
                return "th";

            int mod = rank % 10;
			
            switch (mod)
            {
                case 1:
                    return "st";
                case 2:
                    return "nd";
                case 3:
                    return "rd";
                default:
                    return "th";
            }
        }
		
        public static string ToOrdinal(this int rank)
        {
            return rank + rank.OrdinalSuffix();
        }
    }
}