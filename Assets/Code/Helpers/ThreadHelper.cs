﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Helpers
{
    public class ThreadHelper : MonoBehaviour
    {
        private static ThreadHelper instance;
        private static readonly Queue<Action> executeOnMainThreadQueue = new Queue<Action>();

        private void Awake()
        {
            if (instance != null)
            {
                Debug.LogError("There is more than one ThreadHelper in the scene. Destroying this one.");
                Destroy(this);
                return;
            }
            
            DontDestroyOnLoad(gameObject);
            instance = this;
        }

        public static void ExecuteOnMainThread(Action action)
        {
            lock (executeOnMainThreadQueue)
            {
                executeOnMainThreadQueue.Enqueue(action);
            }
        }

        void Update()
        {
            // dispatch stuff on main thread
            while (executeOnMainThreadQueue.Count > 0)
            {
                Action dequeuedAction = null;
                lock (executeOnMainThreadQueue)
                {
                    try
                    {
                        dequeuedAction = executeOnMainThreadQueue.Dequeue();
                    }
                    catch (Exception e)
                    {
                        Debug.LogException(e);
                    }
                }
                if (dequeuedAction != null)
                {
                    dequeuedAction.Invoke();
                }
            }
        }
    }
}