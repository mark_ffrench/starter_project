﻿using Firebase.Auth;
using Framework;
using Helpers;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class SettingsPopup : UIPopup
    {
        [SerializeField] private TextMeshProUGUI UserIDLabel;
        [SerializeField] private Toggle personalisedAdsToggle;
        [SerializeField] private Toggle notificationsToggle;
        [SerializeField] private Toggle musicToggle;
        
        [SerializeField] private TextMeshProUGUI personalisedAdsLabel;
        [SerializeField] private TextMeshProUGUI notificationsLabel;
        [SerializeField] private TextMeshProUGUI musicLabel;

        [Inject] private AdsProvider adsProvider;
        [Inject] private SoundtrackController soundtrackController;
        
        protected override void OnShow()
        {
            UserIDLabel.text = "User ID: " + AuthProvider.UserID();

            OptInSetting optInSetting = PrivacyAndSettings.GetOptInSetting();
            personalisedAdsToggle.SetIsOnWithoutNotify(optInSetting != OptInSetting.OptOut);
            notificationsToggle.SetIsOnWithoutNotify(PrivacyAndSettings.GetNotificationsSetting());
            musicToggle.SetIsOnWithoutNotify(PrivacyAndSettings.GetMusicSetting());
        }

        public void TogglePersonalisedAds(bool value)
        {
            var setting = value ? OptInSetting.OptIn : OptInSetting.OptOut;
            PrivacyAndSettings.SetOptInSetting(setting);
            adsProvider.ApplyPrivacySettings(setting);
            
            personalisedAdsLabel.text = "Personalised Ads: " + (value ? "On" : "Off");
            Debug.Log("personalised ads: "+setting);
        }
        
        public void ToggleNotifications(bool value)
        {
            PrivacyAndSettings.SetNotificationsSetting(value);
            notificationsLabel.text = "Notifications: " + (value ? "On" : "Off");

            if (value)
            {
                AnalyticsProvider.Get.LogEvent("NotificationsOn");
            }
            else
            {
                AnalyticsProvider.Get.LogEvent("NotificationsOff");
            }

        }
        
        public void ToggleMusic(bool value)
        {
            PrivacyAndSettings.SetMusicSetting(value);
            musicLabel.text = "Music: " + (value ? "On" : "Off");
            
            if (value)
            {
                soundtrackController.StartPlaying();
                AnalyticsProvider.Get.LogEvent("MusicOn");
            }
            else
            {
                soundtrackController.StopPlaying();
                AnalyticsProvider.Get.LogEvent("MusicOff");
            }
        }

        public void OpenTerms()
        {
            PrivacyAndSettings.OpenTermsOfService();
        }
        
        public void OpenPrivacy()
        {
            PrivacyAndSettings.OpenPrivacyPolicy();
        }
        
        public void Close()
        {
            Hide<SettingsPopup>();
        }
        
        public void OpenContact ()
        {
            string userID = AuthProvider.UserID();
            
            string email = "contact@dividetheplunder.com";
            string subject = MyEscapeURL($"Logic Town Contact ({userID})");
            string body = MyEscapeURL($"User ID: {userID}\r\n");
            Application.OpenURL("mailto:" + email + "?subject=" + subject + "&body=" + body);
        }

        public static string MyEscapeURL (string url)
        {
            return WWW.EscapeURL(url).Replace("+","%20");
        }
    }
}