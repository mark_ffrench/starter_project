using Framework;

namespace UI
{
    public class RateTheGamePopup : UIPopup
    {
        //Ask the player if they are enjoying the game, show after a big win.
        
        public void ClickYes()
        {
            Hide<RateTheGamePopup>();
            PrivacyAndSettings.RateGame();
        }

        public void ClickNo()
        {
            Hide<RateTheGamePopup>();
            ShowPopup<NegativeFeedbackPopup>();
        }
    }
}