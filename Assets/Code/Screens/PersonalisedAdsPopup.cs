﻿using System;
using Framework;

namespace UI
{
    public class PersonalisedAdsPopup : UIPopup
    {
        public event Action<bool> OnConfirmConsent;
        
        public void ConfirmConsent(bool consentGiven)
        {
            OnConfirmConsent?.Invoke(consentGiven);
            Hide<PersonalisedAdsPopup>();
        }
    }
}