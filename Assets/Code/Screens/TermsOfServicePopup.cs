﻿using System;
using Framework;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class TermsOfServicePopup : UIPopup
    {
        public event Action<int> OnConfirmConsentWithAge;

        [SerializeField] private Button consentButton;
        [SerializeField] private Slider slider;
        [SerializeField] private TextMeshProUGUI ageLabel;
        
        private int age;

        protected override void OnShow()
        {
            slider.value = 0f;
            SetAge(0f);
        }

        public void OpenTerms()
        {
            PrivacyAndSettings.OpenTermsOfService();
        }
        
        public void OpenPrivacyPolicy()
        {
            PrivacyAndSettings.OpenPrivacyPolicy();
        }

        public void ConfirmConsent()
        {
            OnConfirmConsentWithAge?.Invoke(age);
            Hide<TermsOfServicePopup>();
        }

        public void SetAge(float ageAsFloat)
        {
            age = Mathf.FloorToInt(ageAsFloat);
            ageLabel.text = age.ToString();
            consentButton.interactable = age > 0;
        }
    }
}