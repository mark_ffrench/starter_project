using Framework;
using UnityEngine;

namespace UI
{
    public class NegativeFeedbackPopup : UIPopup
    {
        public void ClickNotNow()
        {
            AnalyticsProvider.Get.LogEvent("SkipFeedback");
            Hide<NegativeFeedbackPopup>();
        }

        public void SendFeedback()
        {
            AnalyticsProvider.Get.LogEvent("SendFeedback");
            Hide<NegativeFeedbackPopup>();
            
            string userID = AuthProvider.UserID();
            string email = "contact@dividetheplunder.com";
            string subject = SettingsPopup.MyEscapeURL($"{Application.productName} Feedback ({userID})");
            string body = SettingsPopup.MyEscapeURL($"User ID: {userID}\r\n");
            
            Application.OpenURL("mailto:" + email + "?subject=" + subject + "&body=" + body);
        }
    }
}