// WARNING: Do not modify! Generated file.

namespace UnityEngine.Purchasing.Security {
    public class AppleTangle
    {
        private static byte[] data = System.Convert.FromBase64String("dC1sfn54YGh+LWxubmh9eWxjbmgACwQni0WL+gAMDAgIDQ6PDAwNUXp6I2x9fWFoI25iYCJsfX1haG5sEpzWE0pd5gjgU3SJIOY7r1pBWOFE1XuSPhlorHqZxCAPDgwNDK6PDAKQMP4mRCUXxfPDuLQD1FMR28Ywpq58n0peWMyiIky+9fbufcDrrkGNGSbdZEqZewTz+WaAI02r+kpAcn1haC1OaH95ZGtkbmx5ZGJjLUx4PRwLDlgJBx4HTH19YWgtRGNuIzwrPSkLDlgJBh4QTH19YWgtTmh/eSEtbmh/eWRrZG5seWgtfWJhZG50Ij2MzgsFJgsMCAgKDw89jLsXjL5UqggEcRpNWxwTed66hi42Sq7YYoYUhNP0RmH4CqYvPQ/lFTP1XQTeCA0OjwwCDT2PDAcPjwwMDemcpAQtYmsteWVoLXllaGMtbH19YWRubF9oYWRsY25oLWJjLXllZH4tbmh/dz2PDHs9AwsOWBACDAzyCQkODwwFJgsMCAgKDwwbE2V5eX1+NyIiepiTdwGpSoZW2Rs6PsbJAkDDGWTcO5RBIHW64IGW0f56lv9733o9QszUO3LMiljUqpS0P0/21dh8k3OsX31haC1fYmJ5LU5MPRMaAD07PTk/MCtqLYc+Z/oAj8LT5q4i9F5nVmlpOC4YRhhUEL6Z+vuRk8Jdt8xVXRs9GQsOWAkOHgBMfX1haC1fYmJ5YWgtRGNuIzwrPSkLDlgJBh4QTH2PDA0LBCeLRYv6bmkIDD2M/z0nC4J+jG3LFlYEIp+/9UlF/W01kxj4PY8Jtj2PDq6tDg8MDw8MDz0ACwRka2RubHlkYmMtTHh5ZWJ/ZHl0PH9sbnlkbmgtfnlseWhgaGN5fiM9PjtXPW88Bj0ECw5YCQseD1hePB4tbGNpLW5of3lka2RubHlkYmMtfQVTPY8MHAsOWBAtCY8MBT2PDAk9LU5MPY8MLz0ACwQni0WL+gAMDAxyTKWV9NzHa5EpZhzdrrbpFifOEgsOWBADCRsJGSbdZEqZewTz+WaAb2FoLX55bGNpbH9pLXlof2B+LWyz+X6W499pAsZ0QjnVrzP0dfJmxWqCBbkt+sahIS1ifbsyDD2Buk7CCQseD1hePB49HAsOWAkHHgdMfX0SiI6IFpQwSjr/pJZNgyHZvJ0f1SNNq/pKQHIFUz0SCw5YEC4JFT0bOD88OT0+O1caAD44PT89ND88OT0K4XA0joZeLd41ybyyl0IHZvIm8boWsJ5PKR8nygIQu0CRU27FRo0aSHMSQWZdm0yEyXlvBh2OTIo+h4wp7+bcun3SAkjsKsf8YHXg6rgaGs1uPnr6NwohW+bXAiwD17d+FEK4pdFzLzjHKNjUAttm2a8pLhz6rKELPQILDlgQHgwM8gkIPQ4MDPI9ELw9VeFXCT+BZb6CENNofvJqU2ixeWVif2R5dDwbPRkLDlgJDh4ATH0ni0WL+gAMDAgIDT1vPAY9BAsOWHlka2RubHloLW90LWxjdC19bH95xBR/+FAD2HJSlv8oDrdYgkBQAPxjaS1uYmNpZHlkYmN+LWJrLXh+aLg3oPkCAw2fBrwsGyN52DEA1m8bXaeH2Nfp8d0ECjq9eHgs");
        private static int[] order = new int[] { 40,47,36,15,27,22,24,32,27,9,10,44,37,52,16,16,41,38,28,35,28,48,58,24,56,28,37,41,34,55,40,44,46,41,45,41,52,38,40,42,50,48,52,49,44,46,51,59,51,53,57,54,55,57,58,59,59,58,58,59,60 };
        private static int key = 13;

        public static readonly bool IsPopulated = true;

        public static byte[] Data() {
        	if (IsPopulated == false)
        		return null;
            return Obfuscator.DeObfuscate(data, order, key);
        }
    }
}
