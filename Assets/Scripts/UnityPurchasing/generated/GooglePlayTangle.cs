// WARNING: Do not modify! Generated file.

namespace UnityEngine.Purchasing.Security {
    public class GooglePlayTangle
    {
        private static byte[] data = System.Convert.FromBase64String("YuHv4NBi4eriYuHh4Fcuo0X368qAOLCJmYbc/cunkEcESQ6PEIN7dtMFpxXHyZxaWdT4r0kmWyeJmI/rpg9CjCLKgJE4CCc0ypUSOK+DgbJbX/2y5hGDjL3PA+Qe+LI97li6Dox6Df3anXko+BDJH83eZG8dRfs/YxrV3/rZzUAUCxzY1uE/uLQ+f1bQYuHC0O3m6cpmqGYX7eHh4eXg4/8p1Xf/PEBWJvaYFbBE07V/JjPiB1Qo4rrXZUJroVKU1mgCPiYiwreA+7H65zs20HC7UYLAomMDTch+BbI3hJmhduAf6voqPlqqVarqf+4XOJQOTe9KeN5n2wvT8gg0HyEFN5pkSBBKf5RwWsBRoT0e5mNhKsKzom0dsw40HTIaU+Lj4eDh");
        private static int[] order = new int[] { 1,4,11,13,9,5,12,9,13,9,12,13,12,13,14 };
        private static int key = 224;

        public static readonly bool IsPopulated = true;

        public static byte[] Data() {
        	if (IsPopulated == false)
        		return null;
            return Obfuscator.DeObfuscate(data, order, key);
        }
    }
}
